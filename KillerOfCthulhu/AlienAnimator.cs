﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using KillerOfCthulhu.Global.ABE;

namespace KillerOfCthulhu
{
    abstract class AlienAnimator
    {

        // Variables
        public enum myDirection { none, left, right, up, down };
        protected myDirection currentDir = myDirection.none;
        protected Texture2D sTexture;
        public Vector2 _position;
        private int frameIndex;
        private double timeElapsed;
        private double timeToUpdate;
        protected string currentAnimation;
        protected Vector2 sDirection = Vector2.Zero;

        // Properties
        public int FramesPerSecond
        {
            set { timeToUpdate = (1f / value); }
        }

        // Collections
        private Dictionary<string, Rectangle[]> sAnimations = new Dictionary<string, Rectangle[]>();
        private Dictionary<string, Vector2> sOffsets = new Dictionary<string, Vector2>();

        public AlienAnimator(Vector2 position)
        {
            _position = position;
        }


        public void AddAnimation(int frames, int yPos, int xStartFrame, string name, int width, int height, Vector2 offset)
        {

            Rectangle[] Rectangles = new Rectangle[frames];

            for (int i = 0; i < frames; i++)
            {
                Rectangles[i] = new Rectangle((i + xStartFrame) * width, yPos, width, height);
            }
            sAnimations.Add(name, Rectangles);
            sOffsets.Add(name, offset);
        }


        public virtual void Update(GameTime gameTime)
        {

            timeElapsed += Time.DeltaTime;


            if (timeElapsed > timeToUpdate)
            {

                timeElapsed -= timeToUpdate;


                if (frameIndex < sAnimations[currentAnimation].Length - 1)
                {
                    frameIndex++;
                }
                else //Restarts the animation
                {
                    AnimationDone(currentAnimation);
                    frameIndex = 0;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sTexture, _position + sOffsets[currentAnimation], sAnimations[currentAnimation][frameIndex], Color.White);
        }


        public void PlayAnimation(string name)
        {
            //Makes sure we won't start a new annimation unless it differs from our current animation
            if (currentAnimation != name && currentDir == myDirection.none)
            {
                currentAnimation = name;
                frameIndex = 0;
            }
        }


        /// Method that is called every time an animation finishes
        /// <param name="animationName">Ended animation</param>
        public abstract void AnimationDone(string animation);
    }
}
