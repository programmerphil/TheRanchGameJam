﻿using KillerOfCthulhu.GameObjectSystem;

namespace KillerOfCthulhu.Player
{
    public interface GameObjectBuilder
    {
        void BuildGameobject();
        void BuildComponents();
        GameObject GetResult();
    }
}