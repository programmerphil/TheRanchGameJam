﻿using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Player;
using Microsoft.Xna.Framework.Content;

namespace KillerOfCthulhu.Rendering.Builders
{
    public abstract class RenderObjectBuilder : GameObjectBuilder
    {
        protected ContentManager _contentManager;

        protected RenderObjectBuilder(ContentManager contentManager)
        {
            _contentManager = contentManager;
        }

        public abstract void BuildGameobject();
        public abstract void BuildComponents();
        public abstract GameObject GetResult();
    }
}