﻿using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Player;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace KillerOfCthulhu.Builders.Abstract
{
    public abstract class SpawnerAndRenderObjectBuilder : GameObjectBuilder
    {
        protected ContentManager _contentManager;
        protected Transform _transformSpawnInfo;
        protected string _tag;
        protected string _name;
        protected GameObject _gameObject;

        protected SpawnerAndRenderObjectBuilder(ContentManager contentManager, Transform transformSpawnInfo, string tag = "", string name = "")
        {
            _contentManager = contentManager;
            _transformSpawnInfo = transformSpawnInfo;
            _tag = tag;
            _name = name;
        }

        public abstract void BuildGameobject();
        public abstract void BuildComponents();
        public abstract GameObject GetResult();
    }
}