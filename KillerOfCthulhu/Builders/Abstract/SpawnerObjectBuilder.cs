﻿using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Player;
using Microsoft.Xna.Framework;

namespace KillerOfCthulhu.Rendering.Builders
{
    public abstract class SpawnerObjectBuilder : GameObjectBuilder
    {
        protected Vector2 _spawnPosition;

        protected SpawnerObjectBuilder(Vector2 spawnPosition)
        {
            _spawnPosition = spawnPosition;
        }

        public abstract void BuildGameobject();
        public abstract void BuildComponents();
        public abstract GameObject GetResult();
    }
}