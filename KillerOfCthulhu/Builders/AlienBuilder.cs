﻿using KillerOfCthulhu.Alien;
using KillerOfCthulhu.Collision;
using KillerOfCthulhu.Components.Rendering;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Global;
using KillerOfCthulhu.Movement;
using KillerOfCthulhu.Tank2;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace KillerOfCthulhu.Builders.Abstract
{
    public class AlienBuilder : SpawnerAndRenderObjectBuilder
    {
        private string _picturePath;
        private Vector2 _shootDirection;
        private AlienType _alienType;

        public AlienBuilder(ContentManager contentManager, Transform transformSpawnInfo, string picturePath,Vector2 shootDirection, AlienType alienType, string tag = "", string name = "") : base(contentManager, transformSpawnInfo, tag, name)
        {
            _picturePath = picturePath;
            _shootDirection = shootDirection;
            _alienType = alienType;
        }

        public override void BuildGameobject()
        {
            _gameObject = new GameObject(new ObjectCreationProperties(_transformSpawnInfo.Position, _transformSpawnInfo.Rotation, tag:_tag));
        }

        public override void BuildComponents()
        {
            _gameObject.AddComponent(new StaticSprite(_contentManager.Load<Texture2D>(_picturePath), Screen.ToWorldSize(0.04f, 0.04f),
                0.5f))
                .AddComponent(new ShotMover(_shootDirection))
                .AddComponent(new Mover(50))
                .AddComponent(new AlienOnDeath())
                .AddComponent(new AlienAnimation(_alienType));
            Timer.Start(1, AddCollisionAfterTime);
        }

        private void AddCollisionAfterTime()
        {
            _gameObject?.AddComponent(new CircleCollider2D(120, true));
        }

        public override GameObject GetResult()
        {
            return _gameObject;
        }
    }
}