﻿using System.Collections.Generic;
using KillerOfCthulhu.GameObjectSystem;

namespace KillerOfCthulhu.Collision
{
    public abstract class Collider : Component
    {
        protected bool _isTrigger;
        protected bool _isColliding;

        public List<Collider> LastColliders;

        protected List<CollisionNameData> MethodNameSet = new List<CollisionNameData>();

        protected Collider(bool isTrigger)
        {
            LastColliders = new List<Collider>();
            _isTrigger = isTrigger;
            MethodNameSet.Add(new CollisionNameData(() => !_isColliding && _isTrigger, CollisionNames.OnTriggerEnter));
            MethodNameSet.Add(new CollisionNameData(() => _isColliding && _isTrigger, CollisionNames.OnTriggerStay));
            MethodNameSet.Add(new CollisionNameData(() => !_isColliding && !_isTrigger, CollisionNames.OnCollisionEnter));
            MethodNameSet.Add(new CollisionNameData(() => _isColliding && !_isTrigger, CollisionNames.OnCollisionStay));
            CollisionManager.Add(this);
        }

        public abstract bool OverlapsWith(Collider otherCollider);

        public virtual void HandleCollision(Collider colliderCollidingWith, MethodCaller methodCaller, bool isExiting)
        {
            if (!isExiting)
            {
                foreach (var methodNameSet in MethodNameSet)
                {
                    if (methodNameSet.CheckFunc())
                    {
                        CallMethod(methodNameSet.MethodName, methodCaller, colliderCollidingWith);
                    }
                }
                if (!_isColliding)
                {
                    _isColliding = true;
                }
            }
            else
            {
                if (_isColliding)
                {
                    if (_isTrigger)
                    {
                        CallMethod(CollisionNames.OnTriggerExit, methodCaller, colliderCollidingWith);
                    }
                    else
                    {
                        CallMethod(CollisionNames.OnCollisionExit, methodCaller, colliderCollidingWith);
                    }
                    _isColliding = false;
                }
            }
        }

        protected void CallMethod(string methodName, MethodCaller methodCaller, Collider colliderCollidingWith)
        {
            methodCaller.Call(methodName, GetComponents<Component>(), colliderCollidingWith);
        }
    }
}