﻿namespace KillerOfCthulhu.Collision
{
    public interface CollisionEnter
    {
        void OnCollisionEnter(Collider otherCollider);
    }
}