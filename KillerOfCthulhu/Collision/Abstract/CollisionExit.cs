﻿namespace KillerOfCthulhu.Collision
{
    public interface CollisionExit
    {
        void OnCollisionExit(Collider otherCollider);
    }
}