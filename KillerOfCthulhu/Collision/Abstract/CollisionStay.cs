﻿namespace KillerOfCthulhu.Collision
{
    public interface CollisionStay
    {
        void OnCollisionStay(Collider otherCollider);
    }
}