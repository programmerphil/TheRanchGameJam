﻿namespace KillerOfCthulhu.Collision
{
    public interface TriggerEnter
    {
        void OnTriggerEnter(Collider otherCollider);
    }
}