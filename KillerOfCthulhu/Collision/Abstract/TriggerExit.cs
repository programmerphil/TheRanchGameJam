﻿namespace KillerOfCthulhu.Collision
{
    public interface TriggerExit
    {
        void OnTriggerExit(Collider otherCollider);
    }
}