﻿namespace KillerOfCthulhu.Collision
{
    public interface TriggerStay
    {
        void OnTriggerStay(Collider otherCollider);
    }
}