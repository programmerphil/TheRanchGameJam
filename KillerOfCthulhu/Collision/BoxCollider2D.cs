﻿using System;
using System.Collections.Generic;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Rendering;
using KillerOfCthulhu.Rendering.Static;
using Microsoft.Xna.Framework;

namespace KillerOfCthulhu.Collision
{
    public class CollisionNameData
    {
        public string MethodName;
        public Func<bool> CheckFunc;

        public CollisionNameData(Func<bool> checkFunc, string methodName)
        {
            MethodName = methodName;
            CheckFunc = checkFunc;
        }
    }

    public class BoxCollider2D : Collider
    {
        public Rectangle Bounds
        {
            get
            {
                if (Transform != null)
                {
                    return new Rectangle((int)Transform.Position.X, (int)Transform.Position.Y, (_sprite?.Width).GetValueOrDefault(), (_sprite?.Height).GetValueOrDefault());
                }
                return default(Rectangle);
            }
        } 

        private Sprite _sprite;

        public BoxCollider2D(bool isTrigger) : base(isTrigger)
        {
        }

        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            _sprite = GetComponent<RenderableComponent>().Sprite;
        }

        public override bool OverlapsWith(Collider otherCollider)
        {
            BoxCollider2D boxCollider2D = otherCollider as BoxCollider2D;
            if (boxCollider2D != null && Bounds.Intersects(boxCollider2D.Bounds))
            {
                return true;
            }
            return false;
        }
    }
}