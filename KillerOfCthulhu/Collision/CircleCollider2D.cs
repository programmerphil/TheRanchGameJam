﻿namespace KillerOfCthulhu.Collision
{
    public class CircleCollider2D : Collider
    {
        public float Radius;

        public CircleCollider2D(float radius, bool isTrigger) : base(isTrigger)
        {
            Radius = radius;
        }

        public override bool OverlapsWith(Collider otherCollider)
        {
            CircleCollider2D otherCircleCollider2D = otherCollider as CircleCollider2D;
            if (otherCircleCollider2D != null)
            {
                float distance = (Transform.Center - otherCircleCollider2D.Transform.Center).Length();
                float sumOfRadii = otherCircleCollider2D.Radius + otherCircleCollider2D.Radius;

                float depth = sumOfRadii - distance;
                if (depth < 0)
                {
                    return false;
                }
                return true;
            }
            return false;
        }
    }
}