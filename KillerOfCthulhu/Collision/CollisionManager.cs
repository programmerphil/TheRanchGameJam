﻿using System.Collections.Generic;
using System.Diagnostics;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.GameObjectSystem.ABE;

namespace KillerOfCthulhu.Collision
{
    public class CollisionManager : Component, Updateable
    {
        private static DelayedList<Collider> _colliders;
        private static MethodCaller _methodCaller;

        static CollisionManager()
        {
           Reset();
        }

        public static void Reset()
        {
            _colliders = new DelayedList<Collider>();
            _methodCaller = new MethodCaller();
        }

        public static void Add(Collider colider)
        {
            _colliders.AddItem(colider);
        }

        public static void Remove(Collider collider)
        {
            _colliders.RemoveItem(collider);
        }

        public void Update()
        {
            _colliders.UpdateList();
            foreach (var collider in _colliders)
            {
                List<Collider> collidersCollding = _colliders.FindAll(item => item.OverlapsWith(collider) && item != collider);

                foreach (var collidingColider in collidersCollding)
                {
                    collidingColider.HandleCollision(collider, _methodCaller, false);
                    collider.HandleCollision(collidingColider, _methodCaller, false);
                }

                if (collider.LastColliders.Count > collidersCollding.Count)
                {
                    foreach (var colliderExiting in collider.LastColliders.FindAll(item => !collidersCollding.Contains(item)))
                    {
                        collider.HandleCollision(colliderExiting, _methodCaller, true);
                    }
                }

                collider.LastColliders.Clear();
                collider.LastColliders.AddRange(collidersCollding);
            }
        }
       
    }
}