﻿namespace KillerOfCthulhu.Collision
{
    public class CollisionNames
    {
        public const string OnCollisionEnter = "OnCollisionEnter";
        public const string OnCollisionStay = "OnCollisionStay";
        public const string OnCollisionExit = "OnCollisionExit";
        public const string OnTriggerEnter = "OnTriggerEnter";
        public const string OnTriggerStay = "OnTriggerStay";
        public const string OnTriggerExit = "OnTriggerExit";
    }
}