﻿using System;
using System.Collections.Generic;
using System.Reflection;
using KillerOfCthulhu.GameObjectSystem;

namespace KillerOfCthulhu.Collision
{
    public class MethodCaller
    {
        private List<string> _allowableMethodNames;
        private List<MethodInfo> _allowableMethods; 

        public MethodCaller()
        {
            _allowableMethods = new List<MethodInfo>();
            _allowableMethodNames = new List<string>()
            {
                CollisionNames.OnCollisionEnter,  CollisionNames.OnCollisionStay,  CollisionNames.OnCollisionExit,
                CollisionNames.OnTriggerEnter,  CollisionNames.OnTriggerStay,  CollisionNames.OnTriggerExit
            };
            FindMethods(Assembly.GetExecutingAssembly());
        }

        public void AddAllowableMethod(string methodName)
        {
            _allowableMethodNames.Add(methodName);
        }

        private void FindMethods(Assembly assembly)
        {
            foreach (var type in assembly.GetTypes())
            {
                foreach (var allowableMethodName in _allowableMethodNames)
                {
                    MethodInfo allowableMethod = type.GetMethod(allowableMethodName);
                    if (allowableMethod != null)
                    {
                        _allowableMethods.Add(allowableMethod);
                    }
                }
            }
        }

        public void Call(string methodName, List<Component> components, params object[] parameters)
        {
            foreach (var method in _allowableMethods)
            {
                if (method.Name == methodName)
                {
                    List<Component> componentsWithMethod = components.FindAll(item => item.GetType() == method.ReflectedType);

                    foreach (var componentWithMethod in componentsWithMethod)
                    {
                        method?.Invoke(componentWithMethod, parameters);
                    }
                }
            }
        }
    }
}