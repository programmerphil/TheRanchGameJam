﻿using System.Diagnostics;
using KillerOfCthulhu.GameObjectSystem;

namespace KillerOfCthulhu.Collision
{
    public class TestCollisionObject : Component, TriggerEnter, TriggerStay, TriggerExit, CollisionEnter, CollisionStay, CollisionExit
    {
        public void OnTriggerEnter(Collider otherCollider)
        {
            Debug.WriteLine("Trigger entered"+ otherCollider);
        }

        public void OnTriggerStay(Collider otherCollider)
        {
            Debug.WriteLine("Trigger stayed" + otherCollider);
        }

        public void OnTriggerExit(Collider otherCollider)
        {
            Debug.WriteLine("Trigger Exited" + otherCollider);
        }

        public void OnCollisionEnter(Collider otherCollider)
        {
            Debug.WriteLine("Collision Entered" + otherCollider);
        }

        public void OnCollisionStay(Collider otherCollider)
        {
            Debug.WriteLine("Collision Stayed" + otherCollider);
        }

        public void OnCollisionExit(Collider otherCollider)
        {
            Debug.WriteLine("Collision Exited" + otherCollider);
        }
    }
}