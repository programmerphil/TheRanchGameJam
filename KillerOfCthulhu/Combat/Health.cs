﻿using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.GameObjectSystem.GameObjects;
using KillerOfCthulhu.Scenes;

namespace KillerOfCthulhu.Combat
{
    public class Health : Component, Updateable
    {
        public int Life { get; set; }

        public void Update()
        {
           ShouldDie();
        }

        private void ShouldDie()
        {
            if (Life <= 0)
            {
                Die();
            }
        }

        private void Die()
        {
            Pool.RemoveGameObject(GameObject);
        }
    }
}