﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KillerOfCthulhu.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using KillerOfCthulhu.Rendering.Static;
using KillerOfCthulhu.GameObjectSystem;

namespace KillerOfCthulhu.Components
{
    class Alien : AlienAnimator, Updateable, Renderable
    {

        static Random _r = new Random();     
        Vector2 _currentDirection = new Vector2(_r.Next(1, 361), _r.Next(1, 361));

        private int speed = 1;
        public Rectangle _rectangle;
        private Texture2D _texture;
        private int monsterID;
        internal int newMonsterID;
        GameTime gameTime;
        private Transform _transform;

        public Alien(int newAlienID, Vector2 position) : base(position)
        {
            monsterID = newMonsterID;
            _transform = new Transform(new TransformCreationProperties(position));
                       FramesPerSecond = 12;

            AddAnimation(90, 0, 0, "AlienCycle", 194, 175, new Vector2(0, 0));
            PlayAnimation("AlienCycle");
        }

        public void LoadContent(ContentManager content)
        {
            _texture = content.Load<Texture2D>("testsheet");
        }

        public void Update()
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            _currentDirection *= speed;

            //_rectangle = new Rectangle(_position.X, _position.Y, _texture.Width, _texture.Height);

            _position += (_currentDirection * deltaTime);

            base.Update(gameTime);
        }

        public override void AnimationDone(string animation)
        {

        }

        public void Render(Graphics graphics)
        {
            graphics.Render(new Sprite(_texture, _transform, size: _rectangle.Size, drawColor: Color.White));
        }
    }
}
