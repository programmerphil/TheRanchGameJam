﻿using KillerOfCthulhu.GameObjectSystem;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KillerOfCthulhu.Global;
using KillerOfCthulhu.Global.ABE;

namespace KillerOfCthulhu.Components
{
    class MonsterSpawner : Component, Updateable
    {
        // Skrymper med tiden ratio falder dog
        // Både den og spillerne har liv, når den er på 15% skal den ik skrympe mere
        // Hver gang timeren er 0 spawn en alien test om den kmmer uden bevægelse

        private const float fullDelay = 3;
        private float currentDelay;
        private bool alive;
        private int newAlienID = 0;
        GameTime gameTime;
        MouseState mouse;

        List<Alien> aliens = new List<Alien>();

        public MonsterSpawner()
        {
            alive = true;
        }

        public void Update()
        {
            float elapsed = Time.DeltaTime;
            currentDelay -= elapsed;

            Rectangle mouseRectangle = new Rectangle(mouse.X, mouse.Y, 1, 1);

            if (currentDelay <= 0)
            {
                aliens.Add(new Alien(newAlienID, new Vector2(Screen.Width / 2, Screen.Height /2)));

                newAlienID += 1;

                currentDelay = fullDelay;   
            }

            foreach ( Alien alien in aliens)
            {
                alien.Update(gameTime);

                if (alien._rectangle.Intersects(mouseRectangle))  // senere erstates med skud/railen
                {
                    aliens.RemoveAt(alien.newMonsterID);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Alien alien in aliens)
            {
                alien.Draw(spriteBatch);
            }
        }
    }
}
