﻿using System;
using System.Collections.Generic;
using ABE.Scenes;
using KillerOfCthulhu.Alien;
using KillerOfCthulhu.Builders.Abstract;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Global;
using KillerOfCthulhu.Player;
using KillerOfCthulhu.Scenes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using SharpDX;
using Component = KillerOfCthulhu.GameObjectSystem.Component;
using Vector2 = Microsoft.Xna.Framework.Vector2;

namespace KillerOfCthulhu.Components
{
    public class MonsterSpawning : Component
    {
        private const float SpawnInterval = 1;
        private GameObjectDirector _gameObjectDirector;
        private ContentManager _contentManager;
        private Dictionary<AlienType, string> PicturePathSet = new Dictionary<AlienType, string>()
        {
            { AlienType.Purple, "purpleAlien"},
            { AlienType.Red, "redAlien"}
        };

        private Random _random;

        public MonsterSpawning(ContentManager contentManager)
        {
            _random = new Random();
            _gameObjectDirector = new GameObjectDirector();
            _contentManager = contentManager;
        }

        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            Spawn();
        }

        public void Spawn()
        {
            if (SceneManager.ActiveScene is TestScene && Transform != null)
            {
                float angle = Transform.Rotation - 90;
                Vector2 direction = GetRandomDirection();
                AlienType randomAlienType = GetRandomAlienType();
                AlienBuilder alienBuilder = new AlienBuilder(_contentManager, new Transform(new TransformCreationProperties(Transform.Position, Transform.Rotation)),
                    GetPath(randomAlienType), direction, randomAlienType, Tag.Enemy);
                GameObject alienObject = _gameObjectDirector.Construct(alienBuilder);
                SceneManager.ActiveScene.Add(alienObject);
                Timer.Start(SpawnInterval, Spawn);
            }
        }

        private AlienType GetRandomAlienType()
        {
            return (AlienType)_random.Next(0, Enum.GetNames(typeof (AlienType)).Length);

        }

        private string GetPath(AlienType alienType)
        {
            return PicturePathSet[alienType];
        }

        private Vector2 GetRandomDirection()
        {
            Vector2 direction = new Vector2(_random.NextFloat(-1, 1), _random.NextFloat(-1, 1));
            direction.Normalize();
            return direction;
        }
    }
}