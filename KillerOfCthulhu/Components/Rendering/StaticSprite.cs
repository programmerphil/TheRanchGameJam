﻿using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Rendering;
using KillerOfCthulhu.Rendering.Static;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace KillerOfCthulhu.Components.Rendering
{
    public class StaticSprite : RenderableComponent
    {
        private Point? _size;
        private float _layerDepth;
        private Sprite _sprite;

        public StaticSprite( Texture2D texture, Point? size = null, float layerDepth = 0) : base(texture)
        {
            _size = size;
            _layerDepth = layerDepth;
        }

        public StaticSprite(Sprite sprite) : base(sprite.Texture)
        {
            _sprite = sprite;
        }

        public override void Init(GameObject gameObject)
        {
            base.Init(gameObject);
            Sprite = _sprite ?? new Sprite(_texture, Transform, size: _size, layerDepth: _layerDepth);
        }
    }
}