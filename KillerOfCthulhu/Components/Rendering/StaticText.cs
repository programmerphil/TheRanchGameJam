﻿using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Rendering;
using KillerOfCthulhu.Rendering.Static;
using KillerOfCthulhu.UI.Buttons;

namespace KillerOfCthulhu.Components.Rendering
{
    public class StaticText : Component, Renderable
    {
        public Text Text;

        public StaticText(Text text)
        {
            Text = text;
        }

        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            Text.Sprite = Text.Sprite ?? GetComponent<RenderableComponent>()?.Sprite;
        }

        public void Render(Graphics graphics)
        {
            graphics.Render(Text);
        }
    }
}