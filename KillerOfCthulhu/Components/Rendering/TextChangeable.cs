﻿namespace KillerOfCthulhu.Components.Rendering
{
    public interface TextChangeable
    {
        object TextValue { get; } 
    }
}