﻿using System.Collections.Generic;
using System.Linq;

namespace KillerOfCthulhu.Extensions
{
    public static class ListExtensions
    {
        public static List<T> ShallowClone<T>(this List<T> listToClone) where T : ShallowCloneable
        {
            return listToClone.Select(item => (T)item.ShallowClone()).ToList();
        }
    }
}