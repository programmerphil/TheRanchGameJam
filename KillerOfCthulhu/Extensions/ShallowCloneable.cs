﻿namespace KillerOfCthulhu.Extensions
{
    public interface ShallowCloneable
    {
        object ShallowClone();
    }
}