﻿using BrandonPotter.XBox;

namespace KillerOfCthulhu.Extensions
{
    public static class XboxInput
    {
        public static XBoxController GetXboxController(int index)
        {
            int controllerIndex = 0;
            foreach (var controller in XBoxController.GetConnectedControllers())
            {
                if (controllerIndex == index)
                {
                    return controller;
                }
                controllerIndex++;
            }
            return null;
        }
    }
}