using System;
using System.Collections.Generic;

namespace KillerOfCthulhu.GameObjectSystem
{
    public abstract class Component
    {
        public GameObject GameObject;
        public Transform Transform;
        public bool Enabled { get; set; }

        protected Component()
        {
            Enabled = true;
        }

        public virtual void Init(GameObject gameObject)
        {
            GameObject = gameObject;
            Transform = gameObject.Transform;
        }

        // These methods are helper methods, added to easy get and add components
        #region Add/Get component methods

        public T GetComponent<T>() where T : Component
        {
            return GameObject.GetComponent<T>();
        }

        public List<T> GetComponents<T>() where T : Component
        {
            return GameObject.GetComponents<T>();
        }

        public T GetComponentInChildren<T>() where T : Component
        {
            return GameObject.GetComponentInChildren<T>();
        }

        public T GetComponentsInChildrenWithTag<T>(string tag) where T : Component
        {
            return GameObject.GetComponentInChildrenWithTag<T>(tag);
        }

        public T GetComponentInParent<T>() where T : Component
        {
            return GameObject.GetComponentInParent<T>();
        }

        public void AddComponent(Component component)
        {
            GameObject.AddComponent(component);
        }

        #endregion

        #region Hooks

        public virtual void OnAddedToGameObject(){ }

        public virtual void OnRemovedFromGameObject()
        {
            
        }

        public virtual void RemoveFromGameObject()
        {
            GameObject = null;
            Transform = null;
            Enabled = false;
        }

        public virtual void OnEnabled()
        {
            Enabled = true;
        }

        public void OnDisabled()
        {
            Enabled = false;
        }
        #endregion

    }
}