using System;
using System.Collections;
using System.Collections.Generic;
using KillerOfCthulhu.GameObjectSystem.ABE;
using KillerOfCthulhu.Rendering;

namespace KillerOfCthulhu.GameObjectSystem
{
    public class ComponentList : IEnumerable<Component>, Animateable
    {
        public event Action AddedComponents;
        public event Action RemovedComponents;

        private DelayedList<Component> _components;

        public ComponentList()
        {
            _components = new DelayedList<Component>();
            _components.AllItemsAdded += OnAddedComponents;
            _components.AllItemsRemoved += OnRemovedComponents;
        }

        private void OnAddedComponents(List<Component> componentsAdded)
        {
            foreach (var component in componentsAdded)
            {
                component.OnAddedToGameObject();
            }
            AddedComponents?.Invoke();
        }

        private void OnRemovedComponents(List<Component> componentsAdded)
        {
            foreach (var component in componentsAdded)
            {
                component.OnRemovedFromGameObject();
            }
            RemovedComponents?.Invoke();
        }

        public Component Add(Component component)
        {
            _components.AddItem(component);
            return component;
        }

        public Component RemoveComponent(Component component)
        {
            _components.RemoveItem(component);
            component.OnRemovedFromGameObject();
            return component;
        }

        public Component RemoveComponent<T>() where T : Component
        {
            Component componentToRemove = _components.GetItem<T>();
            componentToRemove.OnRemovedFromGameObject();
            _components.RemoveItem<T>();
            return componentToRemove;
        }

        public void RemoveComponents<T>() where T : Component
        {
            foreach (var componentToRemove in _components.GetItems<T>())
            {
                componentToRemove.OnRemovedFromGameObject();
            }
            _components.RemoveItems<T>();
        }

        public void RemoveAllComponents()
        {
            foreach (var component in _components)
            {
                component.OnRemovedFromGameObject();
                component.RemoveFromGameObject();
            }
            _components.RemoveAllItems();
        }

        public List<T> GetComponents<T>() where T : Component
        {
            return _components.GetItems<T>();
        }

        public T GetComponent<T>() where T : Component
        {
            return _components.GetItem<T>();
        }

        public void UpdateComponents()
        {
            _components.UpdateList();
            foreach (var component in _components)
            {
                Updateable updateable = component as Updateable;
                updateable?.Update();
            }
        }

        public void RenderComponents(Graphics graphics)
        {
            foreach (var component in _components)
            {
                Renderable renderable = component as Renderable;
                renderable?.Render(graphics);
            }
        }

        public void OnComponentEnabled()
        {
            foreach (var component in _components)
            {
                component.OnEnabled();
            }
        }

        public void OnComponentDisabled()
        {
            foreach (var component in _components)
            {
                component.OnDisabled();
            }
        }

        public void OnAnimationDone(string animationName)
        {
            foreach (var component in _components)
            {
                Animateable animateableComponent = (Animateable) component;
                animateableComponent?.OnAnimationDone(animationName);
            }
        }

        public IEnumerator<Component> GetEnumerator()
        {
            return _components.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}