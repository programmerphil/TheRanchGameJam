using System;
using System.Collections.Generic;
using KillerOfCthulhu.GameObjectSystem.GameObjects;
using KillerOfCthulhu.Rendering;
using Microsoft.Xna.Framework;
using KillerOfCthulhu.GameObjectSystem.GameObjects;
using Object = KillerOfCthulhu.Global.Object;

namespace KillerOfCthulhu.GameObjectSystem
{
    public class GameObject : Updateable, Renderable, Animateable
    {
        public ComponentList Components;
        public Transform Transform;

        public event Action AddedComponents;
        public event Action RemovedComponents;

        private bool _enabled;
        private string _tag;
        private string _name;

        public bool Enabled
        {
            get
            {
                return _enabled;
            }
            set
            {
                if (_enabled != value)
                {
                    _enabled = value;
                    if (!_enabled)
                    {
                        Components.OnComponentDisabled();
                    }
                    if (_enabled)
                    {
                        Components.OnComponentEnabled();
                    }
                    foreach (var child in Transform.Children)
                    {
                        child.GameObject.Enabled = value;
                    }
                }
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                Object.AddName(Name, this);
            }
        }

        public string Tag
        {
            get { return _tag; }
            set
            {
                _tag = value;
                Object.AddTag(_tag, this);
            }
        }

        public GameObject(ObjectCreationProperties objectCreationProperties)
        {
            Init(objectCreationProperties);
        }

        public void Init(ObjectCreationProperties objectCreationProperties)
        {
            Transform = objectCreationProperties.Transform ?? 
                new Transform(objectCreationProperties.TransformCreationProperties);
            Transform.GameObject = this;
            Components = new ComponentList();
            Name = objectCreationProperties.Name;
            Tag = objectCreationProperties.Tag;
            Enabled = true;
            Components.AddedComponents += () => AddedComponents?.Invoke();
            Components.RemovedComponents += () => RemovedComponents?.Invoke();
        }

        public void Update()
        {
            if (Enabled)
            {
                UpdateComponents();
            }
        }

        private void UpdateComponents()
        {
             Components.UpdateComponents();
        }

        public void Render(Graphics graphics)
        {
            if (Enabled)
            {
                Components.RenderComponents(graphics);
            }
        }

        public GameObject AddComponent(Component component)
        {
            component.Init(this);
            Components.Add(component);
            return this;
        }

        public void AddComponents(List<Component> components)
        {
            foreach (var component in components)
            {
                AddComponent(component);
            }
        }

        public T GetComponent<T>() where T : Component
        {
            return Components.GetComponent<T>();
        }

        public List<T> GetComponents<T>() where T : Component
        {
            return Components.GetComponents<T>();
        }

        public T GetComponentInChildren<T>() where T : Component
        {
            return FindComponentInChildren<T>(
                (childTransform => childTransform.GameObject.GetComponent<T>() != null));
        }

        public T GetComponentInChildrenWithTag<T>(string tag) where T : Component
        {
              return FindComponentInChildren<T>(
                    (childTransform => childTransform.GameObject.GetComponent<T>() != null &&
                    childTransform.GameObject.Tag == tag));
        }

        private T FindComponentInChildren<T>(Predicate<Transform> predicate)
            where T : Component
        {
            foreach (var child in Transform.Root.Children)
            {
                var componentInChild = FindComponentInChildren<T>(child, predicate);
                if (componentInChild != null)
                {
                    return componentInChild;
                }
            }

            return null;
        }

        private T FindComponentInChildren<T>(Transform childTranform, Predicate<Transform> predicate) where T : Component
        {
            if (predicate(childTranform))
            {
                return (T)childTranform.GameObject.GetComponent<T>();
            }

            foreach (var child in childTranform.Children)
            {
                Component childComponent = FindComponentInChildren<T>(child, predicate);
                if (childComponent != null)
                {
                    return (T)childComponent;
                }
            }

            return null;
        }

        public T GetComponentInParent<T>() where T : Component
        {
            Transform parent = Transform;

            while (parent != null)
            {
                var component = parent.GameObject.GetComponent<T>();
                if (component != null)
                {
                    return component;
                }
                parent = parent.Parent;
            }

            return null;
        }

        public T RemoveComponent<T>() where T : Component
        {
            return (T)Components.RemoveComponent<T>();
        }

        public void RemoveComponents<T>() where T : Component
        {
            Components.RemoveComponents<T>();
        }

        public Component RemoveComponent(Component component)
        {
            Components.RemoveComponent(component);
            return component;
        }

        public void RemoveAllComponents()
        {
            Components.RemoveAllComponents();
        }

        public void OnAnimationDone(string animationName)
        {
            Components.OnAnimationDone(animationName);
        }

        public void OnRemoved()
        {
            RemoveAllComponents();
            Pool.RemoveGameObject(this);
        }
    }
}