﻿using KillerOfCthulhu.GameObjectSystem;

namespace KillerOfCthulhu.Player
{
    public class GameObjectDirector
    {
        public GameObject Construct(GameObjectBuilder gameObjectBuilder)
        {
            gameObjectBuilder.BuildGameobject();
            gameObjectBuilder.BuildComponents();
            return gameObjectBuilder.GetResult();
        }
    }
}