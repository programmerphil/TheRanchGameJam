﻿using Microsoft.Xna.Framework;

namespace KillerOfCthulhu.GameObjectSystem
{
    public struct ObjectCreationProperties
    {
        public string Name;
        public string Tag;
        public Transform Transform;
        public TransformCreationProperties TransformCreationProperties;

        public ObjectCreationProperties(Vector2 position, float rotation = 0, Vector2? scale = null, string name = "", string tag = "")
        {
            TransformCreationProperties = new TransformCreationProperties(position, rotation, scale);
            Transform = null;
            Name = name;
            Tag = tag;
        }

        public ObjectCreationProperties(TransformCreationProperties transformCreationProperties, string name = "",
            string tag = "")
        {
            Transform = null;
            TransformCreationProperties = transformCreationProperties;
            Name = name;
            Tag = tag;
        }

        public ObjectCreationProperties(Transform transform, string name = null, string tag = null)
        {
            Transform = transform;
            TransformCreationProperties = new TransformCreationProperties();
            Name = name;
            Tag = tag;
        }
    }
}


       