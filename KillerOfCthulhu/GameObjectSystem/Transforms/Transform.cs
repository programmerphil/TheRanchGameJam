using System;
using System.Collections.Generic;
using KillerOfCthulhu.Rendering;
using Microsoft.Xna.Framework;

namespace KillerOfCthulhu.GameObjectSystem
{
    public class Transform
    {
        public GameObject GameObject;

        public Vector2 LocalPosition;
        public float LocalRotation;
        public Vector2 LocalScale;

        private Vector2 _position;
        private float _rotation;
        private Vector2 _scale;
        private List<Transform> _children;
        private Transform _parent;

        public int ChildCount => _children.Count;
        public List<Transform> Children => _children; 

        public Transform Parent
        {
            get { return _parent; }
            set
            {
                UpdatePosition(value);
                _parent?.RemoveChild(this);
                _parent = value;
                _parent?.AddChild(this);
            }
        }

        private void UpdatePosition(Transform newParent)
        {
            if (newParent == null && _parent != null)
            {
                _position = _parent.Position + LocalPosition;
                LocalPosition = Vector2.Zero;
            }
            if (newParent != null && _parent == null)
            {
                LocalPosition += _position - newParent.Position;
                _position = Vector2.Zero;
            }
        }

        public Vector2 Position
        {
            get
            {
                if (Root != this)
                {
                    UpdateTransform(TransformDirtyType.Position);
                }
              
                return _position + LocalPosition;
            }
            set
            {
                LocalPosition = (value - Parent?.Position) ?? LocalPosition;
                _position = Parent != null ? Vector2.Zero : value;
            }
        }
        public float Rotation
        {
            get
            {
                if (Root != this)
                {
                    UpdateTransform(TransformDirtyType.Rotation);
                }
                return _rotation;
            }
            set
            {
                _rotation = value;
                LocalRotation = value;
            }
        }

        public Vector2 Scale
        {
            get
            {
                if (Root != this)
                {
                    UpdateTransform(TransformDirtyType.Scale);
                }
                return _scale;
            }
            set
            {
                _scale = value;
                LocalScale = value;
            }
        }

        public Transform Root
        {
            get
            {
                Transform root = Parent ?? this;

                while (root.Parent != null)
                {
                    root = root.Parent;
                }

                return root;
            }
        }

        public Vector2 Center
        {
            get
            {
                Vector2 center = Position;
                
                RenderableComponent renderableComponent = GameObject?.GetComponent<RenderableComponent>();
                if (renderableComponent != null)
                {
                    center = renderableComponent.Sprite.Center;
                }
               
                return center;
            }
        }

        public Transform()
        {
            LocalPosition = Vector2.Zero;
            LocalRotation = 0;
            LocalScale = Vector2.One;
            _children = new List<Transform>();
        }

        public Transform(TransformCreationProperties transformCreationProperties) : this()
        {
            Rotation = transformCreationProperties.Rotation;
            Position = transformCreationProperties.Position;
            Scale = transformCreationProperties.Scale;
        }

        public Transform GetChild(int index)
        {
            return _children[index];
        }

        public void AddChild(Transform child)
        {
            if (!_children.Contains(child))
            {
                _children.Add(child);
            }
        }

        public void RemoveChild(Transform child)
        {
            _children.Remove(child);
        }

        /// <summary>
        /// Searches from a transform to a transform.
        /// </summary>
        /// <param name="from">Searching from begin transform</param>
        /// <param name="to">Searching to end transform</param>
        /// <returns>The path from beginning transform to the end transform</returns>
        private List<Transform> GetPathToTransform(Transform begin, Transform end)
        {
            List<Transform> path = new List<Transform>();
            Transform currentTransform = begin;

            while (currentTransform != end && currentTransform.Parent != null)
            {
                path.Add(currentTransform);
                currentTransform = currentTransform.Parent;
            }

            return path;
        }

        private void UpdateTransform(TransformDirtyType transformDirtyType)
        {
            List<Transform> pathToTranform = GetPathToTransform(this, Root);
            pathToTranform.Reverse();

            if (transformDirtyType.HasFlag(TransformDirtyType.Scale))
            {
                Vector2 newScale = Root.Scale;
                foreach (var transform in pathToTranform)
                {
                    newScale *= transform.LocalScale;
                }
                _scale = newScale;
            }
            if (transformDirtyType.HasFlag(TransformDirtyType.Position))
            {
                Vector2 newPosition = Root.Position;
                foreach (var transform in pathToTranform)
                {
                    newPosition += transform.LocalPosition;
                }
                _position = newPosition;
            }
            if (transformDirtyType.HasFlag(TransformDirtyType.Rotation))
            {
                float newRotation = Root.Rotation;
                foreach (var transform in pathToTranform)
                {
                    newRotation += transform.LocalRotation;
                }
                _rotation = newRotation;
            }
        }
    }
}