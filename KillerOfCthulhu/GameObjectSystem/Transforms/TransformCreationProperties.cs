﻿using Microsoft.Xna.Framework;

namespace KillerOfCthulhu.GameObjectSystem
{
    public struct TransformCreationProperties
    {
        public Vector2 Position;
        public float Rotation;
        public Vector2 Scale;

        public TransformCreationProperties(Vector2 position, float rotation = 0, Vector2? scale = null)
        {
            Position = position;
            Rotation = rotation;
            Scale = scale.GetValueOrDefault(Vector2.One);
        }
    }
}