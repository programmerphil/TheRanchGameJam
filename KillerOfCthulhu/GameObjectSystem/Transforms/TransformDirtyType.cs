﻿using System;

namespace KillerOfCthulhu.GameObjectSystem
{
    [Flags]
    public enum TransformDirtyType
    {
        Scale = 1,
        Position = 2,
        Rotation = 4
    }
}