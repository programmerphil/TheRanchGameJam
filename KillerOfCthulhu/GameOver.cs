﻿using System;
using ABE.Scenes;
using KillerOfCthulhu.Components.Rendering;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.GameObjectSystem.GameObjects;
using KillerOfCthulhu.Global;
using KillerOfCthulhu.Manager;
using KillerOfCthulhu.Rendering.Static;
using KillerOfCthulhu.Scenes;
using KillerOfCthulhu.Tank;

namespace KillerOfCthulhu
{
    public class GameOver : Component, Updateable
    {
        public void Update()
        {
            if (HealthManager.Health <= 0)
            {
                string winningText = GetWinningText();
                SceneManager.Load("GameOver");
                GameObject winingTextObject =
                    Pool.CreateGameObject(new ObjectCreationProperties(Screen.ToWorldPosition(0.35f, 0.4f)));
                winingTextObject.AddComponent(
                    new StaticText(new Text(winningText + Environment.NewLine + "Press A to restart",
                        winingTextObject.Transform)));
                SceneManager.ActiveScene.Add(winingTextObject);
            }
        }

        private string GetWinningText()
        {
            if (TestScene.Player1Score.Score > TestScene.Player2Score.Score)
            {
                SoundManager.PlaySoundEffect("Player 1 wins");
                return "Player 1 wins";
            }
            if (TestScene.Player1Score.Score < TestScene.Player2Score.Score)
            {
                SoundManager.PlaySoundEffect("Player 2 wins");
                return "Player 2 wins";
            }
            if (TestScene.Player1Score.Score == TestScene.Player2Score.Score)
            {
                return "Tie";
            }

            return "";
        }
    }
}