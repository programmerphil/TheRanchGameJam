﻿using KillerOfCthulhu;

namespace KillerOfCthulhu.Global
{
    public static class GameManager
    {
        private static GameWorld _gameWorld;

        public static void SetGameWorld(GameWorld gameWorld)
        {
            _gameWorld = gameWorld;
        }

        public static void Exit()
        {
            _gameWorld.ExitGame();
        }
    }
}