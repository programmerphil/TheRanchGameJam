using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.GameObjectSystem.ABE;
using KillerOfCthulhu.GameObjectSystem.GameObjects;
using KillerOfCthulhu.Scenes;
using Microsoft.Xna.Framework;

namespace KillerOfCthulhu.Global
{
    public static class Object
    {
        private static Dictionary<string, GameObject> _objectsWithName;
        private static Dictionary<string, GameObject> _objectsWithTag;

        static Object()
        {
            _objectsWithName = new Dictionary<string, GameObject>();
            _objectsWithTag = new Dictionary<string, GameObject>();
            SceneManager.StartedLoadingScene += (newScene) => Clear();
        }

        public static GameObject FindGameObjectWithTag(string tag)
        {
            return FindObjectInListWithText(_objectsWithTag, tag);
        }

        public static List<GameObject> FindGameObjectsWithTag(string tag)
        {
            return FindObjectsInListWithText(_objectsWithTag, tag);
        }

        public static GameObject FindGameObject(string name)
        {
            return FindObjectInListWithText(_objectsWithName, name);
        }

        public static List<GameObject> FindGameObjects(string name)
        {
            return FindObjectsInListWithText(_objectsWithName, name);
        }

        private static GameObject FindObjectInListWithText(Dictionary<string, GameObject> gameObjectSet, string textToFind)
        {
            if (gameObjectSet.ContainsKey(textToFind))
            {
                return gameObjectSet[textToFind];
            }
            return Pool.CreateGameObject(new ObjectCreationProperties(new Vector2(0, 0)));
        }

        private static List<GameObject> FindObjectsInListWithText(Dictionary<string, GameObject> gameObjectSet, string textToFind)
        {
            List<GameObject> gameObjectsWithTag = new List<GameObject>();

            if (gameObjectSet.ContainsKey(textToFind))
            {
                gameObjectsWithTag.Add(FindObjectInListWithText(gameObjectSet, textToFind));
            }

            return gameObjectsWithTag;
        }

        public static void AddTag(string tag, GameObject gameObject)
        {
            if (!string.IsNullOrEmpty(tag) && !_objectsWithTag.ContainsKey(tag))
            {
                _objectsWithTag.Add(tag, gameObject);
            }
        }

        public static void AddName(string name, GameObject gameObject)
        {
            if (!string.IsNullOrEmpty(name) && !_objectsWithName.ContainsKey(name))
            {
                _objectsWithName.Add(name, gameObject);
            }
        }

        public static void Remove(GameObject gameObject)
        {
            if (!string.IsNullOrEmpty(gameObject.Tag))
            {
                _objectsWithTag.Remove(gameObject.Tag);
            }
            if (!string.IsNullOrEmpty(gameObject.Name))
            {
                _objectsWithName.Remove(gameObject.Name);
            }
        }

        private static void Clear()
        {
            _objectsWithName.Clear();
            _objectsWithTag.Clear();
        }
    }
}