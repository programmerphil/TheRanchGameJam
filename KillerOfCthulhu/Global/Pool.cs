﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace KillerOfCthulhu.GameObjectSystem.GameObjects
{
    public static class Pool
    {
        private const int StartPoolObjectCount = 100; 

        private static Queue<GameObject> _inActiveGameObjects;

        static Pool()
        {
            _inActiveGameObjects = new Queue<GameObject>();
            SetupPool();
        }

        private static void SetupPool()
        {
            for (int i = 0; i < StartPoolObjectCount; i++)
            {
                GameObject gameObject = new GameObject(new ObjectCreationProperties(Vector2.Zero));
                DeactivateGameObject(gameObject);
                _inActiveGameObjects.Enqueue(gameObject);
            }
        }

        public static GameObject CreateGameObject(ObjectCreationProperties objectCreationProperties)
        {
            if (_inActiveGameObjects.Count > 0)
            {
                GameObject gameObject = _inActiveGameObjects.Dequeue();
                gameObject.Init(objectCreationProperties);
                ActivateGameObject(gameObject);
                return gameObject;
            }
          
            return new GameObject(objectCreationProperties);
        }

        public static void RemoveGameObject(GameObject gameObject)
        {
            DeactivateGameObject(gameObject);
            _inActiveGameObjects.Enqueue(gameObject);
        }

        private static void DeactivateGameObject(GameObject gameObject)
        {
            gameObject.Enabled = false;
        }

        private static void ActivateGameObject(GameObject gameObject)
        {
            gameObject.Enabled = true;
        }
    }
}