﻿using System;
using KillerOfCthulhu.Scenes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KillerOfCthulhu.Global
{
    public static class Screen
    {
        public static Point CurrentScreenResolution => new Point(ViewPort.Width, ViewPort.Height);
        public static int Width => CurrentScreenResolution.X;
        public static int Height => CurrentScreenResolution.Y;

        public static Viewport ViewPort;

        public static void Init(Viewport viewport)
        {
            ViewPort = viewport;
        }

        public static Vector2 ToScreenPosition(Vector2 worldPosition)
        {
            return worldPosition / new Vector2(Width, Height);
        }
        public static Vector2 ToWorldPosition(Vector2 screenPosition)
        {
            return screenPosition * new Vector2(Width, Height);
        }

        public static Vector2 ToScreenPosition(float x, float y)
        {
            return ToScreenPosition(new Vector2(x, y));
        }
        public static Vector2 ToWorldPosition(float x, float y)
        {
            return ToWorldPosition(new Vector2(x, y));
        }

        public static Point ToScreenSize(float x, float y)
        {
            return ToScreenPosition(x, y).ToPoint();
        }

        public static Point ToWorldSize(float x, float y)
        {
            return ToWorldPosition(x, y).ToPoint();
        }
    }
}