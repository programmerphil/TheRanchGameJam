﻿namespace KillerOfCthulhu.Global
{
    public static class Tag
    {
        public const string Shot = "Shot";
        public const string Player = "Player";
        public const string Enemy = "Enemy";
    }
}