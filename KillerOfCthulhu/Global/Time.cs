namespace KillerOfCthulhu.Global
{
    namespace ABE
    {
        public static class Time
        {
            /// <summary>
            /// Is influenced by timescale. 
            /// They are multiplied together
            /// Example :
            /// Timescale = 1. Deltatime = 0.1.
            /// DeltaTime after influenced = 0.1.
            /// Timescale = 0.5 Deltatime = 0.1.
            /// Deltaime after influenced = 0.05
            /// Timescale = 1.5 Deltatime = 0.1
            /// Deltatime after influenced = 0.15.
            /// </summary>
            public static float DeltaTime
            {
                get { return TimeScale * _deltaTime; }
                set
                {
                    _deltaTime = value;
                    TimeSinceStart += _deltaTime;
                }
            }
            public static float TimeSinceStart;
            public static float TimeScale = 1;

            private static float _deltaTime;
        }
    }
}