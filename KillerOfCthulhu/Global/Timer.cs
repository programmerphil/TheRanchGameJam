﻿using System;
using System.Collections.Generic;

namespace KillerOfCthulhu.Global
{
    public class TimerCallback
    {
        private Action _callback;
        private Action<float> _tickCallback;

        private System.Timers.Timer _dispatcherTimer;
        private float _timeGone;
        private float _duration;
        private float _tickInterval;

        public TimerCallback(float duration, Action callback, float tickInterval = 0.1f, Action<float> tickCallback = null)
        {
            _callback = callback;
            _duration = duration;
            _tickInterval = tickInterval;
            _tickCallback = tickCallback;
            _timeGone = 0;
            _dispatcherTimer = new System.Timers.Timer();
            _dispatcherTimer.Interval = tickInterval * 1000f;
            _dispatcherTimer.Elapsed += ShouldCallback; 
            _dispatcherTimer.Start();
        }

        private void ShouldCallback(object sender, object o)
        {
            _timeGone += _tickInterval;
            _tickCallback?.Invoke(_timeGone);
            if (_timeGone >= _duration)
            {
                Callback();
            }
        }

        private void Callback()
        {
            _callback?.Invoke();
            _dispatcherTimer.Stop();
        }
    }

    public static class Timer
    {
        private static List<TimerCallback> _timersCallback = new List<TimerCallback>(); 
        
        /// <summary>
        /// A timer, that calls back, when its duration is over.
        /// </summary>
        /// <param name="duration">How long the timer is in secounds</param>
        /// <param name="callbackAction">Which action to take, when the timer duration is over.</param>
        public static void Start(float duration, Action callbackAction)
        {
            _timersCallback.Add(new TimerCallback(duration, callbackAction));
        }

        public static void Start(float duration, Action callbackAction, float tickInterval, Action<float> tickAction)
        {
            _timersCallback.Add(new TimerCallback(duration, callbackAction, tickInterval, tickAction));
        }

        public static void Start(float duration, float tickInterval, Action<float> tickAction)
        {
            _timersCallback.Add(new TimerCallback(duration, null, tickInterval, tickAction));
        }
    }
}