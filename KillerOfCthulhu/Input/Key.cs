﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;

namespace KillerOfCthulhu.Input
{
    public class KeyEventArgs : EventArgs
    {
        public Keys Key;

        public KeyEventArgs(Keys key)
        {
            Key = key;
        }
    }

    public static class Key
    {
        public static event EventHandler<KeyEventArgs> KeyDown;
        public static event EventHandler<KeyEventArgs> KeyUp;

        private static List<Keys> _lastKeysDown;

        static Key()
        {
            _lastKeysDown = new List<Keys>();
        }

        public static void Update()
        {
            CheckForKeyEvents();
            _lastKeysDown.Clear();
            _lastKeysDown.AddRange(Keyboard.GetState().GetPressedKeys());
        }

        private static void CheckForKeyEvents()
        {
            KeyboardState keyboardState = Keyboard.GetState();
            List<Keys> keysDown = new List<Keys>();
            keysDown.AddRange(keyboardState.GetPressedKeys());
            if (keysDown.Count < _lastKeysDown.Count)
            {
                foreach (var releasedKey in _lastKeysDown.FindAll(item => !keysDown.Contains(item)))
                {
                    KeyUp?.Invoke(null, new KeyEventArgs(releasedKey));
                }
            }
            if (keysDown.Count > _lastKeysDown.Count)
            {
                foreach (var pressedKey in keysDown.FindAll(item => !_lastKeysDown.Contains(item)))
                {
                    KeyDown?.Invoke(null, new KeyEventArgs(pressedKey));
                }
            }
        }
    }
}