﻿using KillerOfCthulhu.Components.Rendering;
using KillerOfCthulhu.Inventory.Display;

namespace KillerOfCthulhu.Manager
{
    public class HealthManager : TextChangeable
    {
        public const int StartHealth = 10;

        public object TextValue => Health;
        public static int Health = StartHealth;
    }
}