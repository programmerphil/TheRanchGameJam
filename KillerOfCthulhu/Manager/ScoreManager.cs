﻿using System;
using KillerOfCthulhu.Components.Rendering;
using KillerOfCthulhu.Tank;

namespace KillerOfCthulhu.Manager
{
    public class ScoreManager : TextChangeable
    {
        public int Score;

        public object TextValue => Score;
    }
}