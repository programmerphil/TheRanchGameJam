﻿using System;
using System.Collections.Generic;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Rendering;

namespace KillerOfCthulhu.Alien
{
    public enum AlienType
    {
        Red = 0,
        Purple = 1
    }

    public class AlienAnimation  : Component
    {
        private AlienType _alienType;

        public AlienAnimation(AlienType alienType)
        {
            _alienType = alienType;
        }

        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            Animator animator = new Animator();
            AddComponent(animator);
            switch (_alienType)
            {
                case AlienType.Red:
                    animator.CreateAnimation(new SpriteSheetAnimation(animator, "Run", 12, 175, 175, "redAlien", new SpriteSheetData(0, 0, 11),
                        new SpriteSheetData(0, 0, 11), new SpriteSheetData(0, 0, 11), new SpriteSheetData(0, 0, 11), new SpriteSheetData(0, 0, 11)
                        , new SpriteSheetData(0, 0, 11), new SpriteSheetData(0, 0, 11), new SpriteSheetData(0, 0, 11), new SpriteSheetData(0, 0, 2)));
                    animator.CreateAnimation(new SpriteSheetAnimation(animator, "Default", 12, 175, 175, "redAlien", new SpriteSheetData(0, 0, 1)));
                    break;
                case AlienType.Purple:
                    animator.CreateAnimation(new SpriteSheetAnimation(animator, "Run", 12, 175, 175, "purpleAlien", new SpriteSheetData(0, 0, 10),
                        new SpriteSheetData(0, 0, 10), new SpriteSheetData(0, 0, 10), new SpriteSheetData(0, 0, 10), new SpriteSheetData(0, 0, 10)
                        ,new SpriteSheetData(0, 0, 10), new SpriteSheetData(0, 0, 10), new SpriteSheetData(0, 0, 10)));
                    animator.CreateAnimation(new SpriteSheetAnimation(animator, "Default", 12, 175, 175, "purpleAlien", new SpriteSheetData(0, 0, 1)));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
           animator.PlayAnimation("Run");
        }
    }
}