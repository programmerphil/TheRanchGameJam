﻿using KillerOfCthulhu.Collision;
using KillerOfCthulhu.Combat;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Global;
using KillerOfCthulhu.Manager;
using KillerOfCthulhu.Scenes;

namespace KillerOfCthulhu.Alien
{
    public class AlienDamageTrigger : Component, TriggerExit
    {
        public void OnTriggerExit(Collider otherCollider)
        {
            if (otherCollider.GameObject != null && otherCollider.GameObject.Tag == Tag.Enemy)
            {
                HealthManager.Health -= 1;
                SceneManager.ActiveScene.Remove(otherCollider.GameObject);
            }
        }
    }
}