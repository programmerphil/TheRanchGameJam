﻿using KillerOfCthulhu.Components.Rendering;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.GameObjectSystem.GameObjects;
using KillerOfCthulhu.Rendering;
using KillerOfCthulhu.Scenes;
using Microsoft.Xna.Framework.Graphics;

namespace KillerOfCthulhu.Alien
{
    public class AlienOnDeath : Component
    {
        public override void OnRemovedFromGameObject()
        {
            base.OnRemovedFromGameObject();
            if (Transform != null)
            {
                Animator animator = new Animator();
                GameObject splat = Pool.CreateGameObject(new ObjectCreationProperties(Transform.Position));
                splat.AddComponent(new StaticSprite(SceneManager.ActiveScene.ContentManager.Load<Texture2D>("splat"), GetComponent<RenderableComponent>().Sprite.Size, layerDepth: 0.3f))
                    .AddComponent(animator);
                animator.CreateAnimation(new SpriteSheetAnimation(animator, "splat", 12, 350, 350, "", new SpriteSheetData(0, 0, 5), new SpriteSheetData(0, 0, 1)));
                animator.PlayAnimation("splat", (animationName) => SceneManager.ActiveScene.Remove(splat));
                SceneManager.ActiveScene.Add(splat);
                SoundManager.PlaySoundEffect("Alien Splat");
            }
          
        }
    }
}