﻿using System;
using System.Linq;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Rendering;

namespace KillerOfCthulhu.Alien
{
    public class Boss : Component
    {
        private Animator _animator;
        private Random _random;

        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            _animator = GetComponent<Animator>();
            _random = new Random();
            PlayRandomAnimation();
        }

        public void PlayRandomAnimation()
        {
            _animator.PlayAnimation(_animator.Animations.Keys.ElementAt(_random.Next(0, _animator.Animations.Count)), animationName => PlayRandomAnimation());
        }
    }
}