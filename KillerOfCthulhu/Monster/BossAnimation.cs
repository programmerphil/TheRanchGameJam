﻿using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Rendering;

namespace KillerOfCthulhu.Alien
{
    public class BossAnimation : Component
    {
        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            Animator animator = new Animator();
            AddComponent(animator);
            animator.CreateAnimation(new SpriteSheetAnimation(animator, "Pulse", 12, 747, 747, "monster-pulse", new SpriteSheetData(0, 0, 5),
                new SpriteSheetData(0, 0, 5), new SpriteSheetData(0, 0, 5), new SpriteSheetData(0, 0, 5)));
            animator.CreateAnimation(new SpriteSheetAnimation(animator, "Left", 12, 747, 747, "monster-looking-left", new SpriteSheetData(0, 0, 8),
                new SpriteSheetData(0, 0, 8), new SpriteSheetData(0, 0, 8), new SpriteSheetData(0, 0, 8), new SpriteSheetData(0, 0, 8),
                new SpriteSheetData(0, 0, 8), new SpriteSheetData(0, 0, 8), new SpriteSheetData(0, 0, 7)));
            animator.CreateAnimation(new SpriteSheetAnimation(animator, "Right", 12, 747, 747, "monster-looking-right", new SpriteSheetData(0, 0, 8),
                new SpriteSheetData(0, 0, 8), new SpriteSheetData(0, 0, 8), new SpriteSheetData(0, 0, 8), new SpriteSheetData(0, 0, 8),
                new SpriteSheetData(0, 0, 8), new SpriteSheetData(0, 0, 8), new SpriteSheetData(0, 0, 7)));
            animator.CreateAnimation(new SpriteSheetAnimation(animator, "Up", 12, 747, 747, "monster-looking-up", new SpriteSheetData(0, 0, 10),
                new SpriteSheetData(0, 0, 10), new SpriteSheetData(0, 0, 10), new SpriteSheetData(0, 0, 10), new SpriteSheetData(0, 0, 10),
                new SpriteSheetData(0, 0, 3)));
            animator.CreateAnimation(new SpriteSheetAnimation(animator, "Blinker", 12, 747, 747, "blinker", new SpriteSheetData(0, 0, 5),
                new SpriteSheetData(0, 0, 5), new SpriteSheetData(0, 0, 3)));
            
        }
    }
}