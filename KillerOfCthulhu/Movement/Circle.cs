﻿using System;
using System.Collections.Generic;
using System.Linq;
using KillerOfCthulhu.GameObjectSystem;
using Microsoft.Xna.Framework;

namespace ABE.Movement
{
    public class Circle : Component
    {
        private const float AnglePerRound = 0.1f;
        private const int CircleMaxDegree = 360;
        private const int StartRadius = 200;
        private const float SelfDistanceThreeshold = 5;

        public readonly List<Vector2> Points;

        public Vector2 Center;
        public int Radius;

        public Circle(Vector2 center, int radius = StartRadius)
        {
            Center = center;
            Radius = radius;
            Points = new List<Vector2>();
            CalculatePoints();
        }

        public Vector2 FindClosestPoint(Vector2 checkPoint, bool includeSelf = false)
        {
            float biggestDistance = float.MaxValue;
            Vector2 closestPoint = Vector2.Zero;
            foreach (var point in Points)
            {
                float distance = Vector2.Distance(checkPoint, point);
                if (distance < biggestDistance && (Vector2.Distance(point, checkPoint) > SelfDistanceThreeshold || includeSelf))
                {
                    biggestDistance = distance;
                    closestPoint = point;
                }
            }
            return closestPoint;}

        private void CalculatePoints()
        {
            for (float currentAngle = 0; currentAngle <= CircleMaxDegree; currentAngle += AnglePerRound)
            {
                Points.Add(CalculatePointOnCircle(currentAngle));
            }
        }

        private Vector2 CalculatePointOnCircle(float angle)
        {
            return Center + new Vector2(Radius * (float)Math.Cos(angle), Radius * (float)Math.Sin(angle));
        }
    }
}