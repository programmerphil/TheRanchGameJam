﻿using System.Collections.Generic;
using ABE.Movement;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace KillerOfCthulhu.Movement
{
    public class CircleMover : Component, Moveable
    {
        private const float LengthCheck = 25;

        private PointMover _pointMover;
        private Circle _circle;
        private Dictionary<List<Keys>, Vector2> InputSet = new Dictionary<List<Keys>, Vector2>()
        {
            { new List<Keys>()  {Keys.A}, -Vector2.UnitX },
            { new List<Keys>() {Keys.D}, Vector2.UnitX },
            { new List<Keys>() {Keys.S}, Vector2.UnitY },
            { new List<Keys>() {Keys.W}, -Vector2.UnitY }
        };

        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            _circle = GetComponent<Circle>();
            _pointMover = GetComponent<PointMover>();
        }

        private void OnKeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            foreach (var input in InputSet)
            {
                if(input.Key.Exists(item => item == keyEventArgs.Key))
                {
                    Move(input.Value);
                }
            }
        }

        public void Stop()
        {
            _pointMover.Stop();
        }

        public void Move(Vector2 direction)
        {
            direction.Normalize();
            Vector2 movedPosition = Transform.Center + (direction * LengthCheck);
            Vector2 closestPointOnCircle = _circle.FindClosestPoint(movedPosition);
            _pointMover.Points = new List<Vector2>() { closestPointOnCircle};
            _pointMover.MoveTo(0);
        } 
    }
}
