﻿using KillerOfCthulhu.GameObjectSystem;
using Microsoft.Xna.Framework;

namespace KillerOfCthulhu.Movement
{
    public interface Moveable
    {
        void Stop();
    }
}