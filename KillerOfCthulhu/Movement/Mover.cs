﻿using System;
using System.Diagnostics;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Global.ABE;
using KillerOfCthulhu.Rendering;
using Microsoft.Xna.Framework;

namespace KillerOfCthulhu.Movement
{
    public class Mover : Component, Moveable, Updateable
    {
        public const float MovementStopThreeshold = 2;
        public float Speed;

        public event Action ReachedTarget;

        private Transform _target;
        private bool _hasReachedTarget;
        private bool _canMove;

        public Mover( float speed)
        {
            Speed = speed;
            StartMoving();
        }

        public void MoveTo(Transform target)
        {
            _target = target;
            _hasReachedTarget = false;
            StartMoving();
        }

        public void Stop()
        {
            _canMove = false;
        }

        public void StartMoving()
        {
            _canMove = true;
        }

        public void ClearReachedTarget()
        {
            ReachedTarget = null;
        }

        public void Update()
        {
            if (_target != null && _canMove && !_hasReachedTarget)
            {
                Vector2 myCenter = Transform.Position + GetCenterOffset(GameObject);
                Vector2 targetCenter = _target.Position +  GetCenterOffset(_target.GameObject);

                if (Vector2.Distance(targetCenter, myCenter) > MovementStopThreeshold)
                {
                    Transform.Position += DirectionToTargetNormalized(myCenter, targetCenter) * Speed * Time.DeltaTime;
                }
                else               
                {
                    _hasReachedTarget = true;
                    ReachedTarget?.Invoke();
                }
            }
        }

        private Vector2 GetCenterOffset(GameObject gameObject)
        {
            Vector2 centerOffset = Vector2.Zero;

            if (gameObject != null)
            {
                RenderableComponent renderableComponent = gameObject.GetComponent<RenderableComponent>();
                centerOffset = renderableComponent?.Sprite.Center - renderableComponent?.Transform.Position ?? Vector2.Zero;
            }

            return centerOffset;
        }

        private Vector2 DirectionToTargetNormalized(Vector2 from, Vector2 target)
        { 
            Vector2 directionToTarget = (target - from);
            directionToTarget.Normalize();
            return directionToTarget;
        }
    }
}