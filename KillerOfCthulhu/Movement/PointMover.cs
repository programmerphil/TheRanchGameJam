﻿using System.Collections.Generic;
using KillerOfCthulhu;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Movement;
using Microsoft.Xna.Framework;

namespace ABE.Movement
{
    public class PointMover : Component, Updateable, Moveable
    {
        public List<Vector2> Points;

        private Mover _mover;
        private int _index;

        public PointMover(List<Vector2> points)
        {
            Points = points ?? new List<Vector2>();
        }

        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            _mover = GetComponent<Mover>();
        }

        public void AddPoints(List<Vector2> points)
        {
            Points.AddRange(points);
        }

        public void AddPoint(Vector2 point)
        {
            Points.Add(point);
        }

        private void Next()
        {
            _index++;
            if (_index >= Points.Count)
            {
                _index = 0;
            }
        }

        public void MoveTo(int index)
        {
            _mover.MoveTo(new Transform(new TransformCreationProperties(Points[index])));
        }

        public void MoveToNewPoint()
        {
            Next();
            MoveTo(_index);
        }

        public void Update()
        {
            _mover.Update();
        }

        public void Stop()
        {
            _mover.Stop();
        }
    }
}