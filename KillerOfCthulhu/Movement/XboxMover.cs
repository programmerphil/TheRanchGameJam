﻿using System;
using System.Diagnostics;
using BrandonPotter.XBox;
using KillerOfCthulhu.GameObjectSystem;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace KillerOfCthulhu.Movement
{
    public class XboxMover : Component, Updateable
    {
        private int _controllerIndex;

        public XboxMover(int controllerIndex)
        {
            _controllerIndex = controllerIndex;
        }

        private CircleMover _circleMover;

        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            _circleMover = GetComponent<CircleMover>();
           
        }

        public void Update()
        {

            XBoxController xBoxController = GetXboxController(_controllerIndex);
            if (xBoxController != null)
            {
                Vector2 amountToMove = new Vector2(((float)xBoxController.ThumbLeftX - 50), -((float)xBoxController.ThumbLeftY - 50));
                Debug.WriteLine("Amount to move"+amountToMove);
                if (amountToMove.LengthSquared() > 10)
                {
                    _circleMover.Move(amountToMove);
                }
                else
                {
                    _circleMover.Stop();
                }
            }
        }

        private XBoxController GetXboxController(int index)
        {
            int controllerIndex = 0;
            foreach (var controller in XBoxController.GetConnectedControllers())
            {
                if (controllerIndex == index)
                {
                    return controller;
                }
                controllerIndex++;
            }
            return null;
        }
    }
}