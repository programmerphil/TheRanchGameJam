namespace KillerOfCthulhu.Rendering
{
    public interface Animateable
    {
        void OnAnimationDone(string animationName);
    }
}