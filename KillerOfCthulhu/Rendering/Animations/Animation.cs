using KillerOfCthulhu;
using KillerOfCthulhu.Global.ABE;

namespace KillerOfCthulhu.Rendering
{

    public abstract class Animation : Updateable
    {
        public string Name;
        public string Path;

        protected int _fps;
        protected int _currentIndex;
        protected RenderableComponent _renderableComponent;
        protected float _timeElapsed;
        protected Animator _animator;
        protected int _maxAnimationCount;

        protected Animation(Animator animator, string name, int fps, string path = "")
        {
            _renderableComponent = animator.GameObject.GetComponent<RenderableComponent>();
            Name = name;
            _fps = fps;
            _animator = animator;
            Path = path;
        }

        public virtual void Update()
        {
            _timeElapsed += Time.DeltaTime;
            _currentIndex = (int)(_fps*_timeElapsed);
            if (_currentIndex >= _maxAnimationCount)
            {
                Reset();
                OnEnd();
            }
        }

        protected void Reset()
        {
            _timeElapsed = 0;
            _currentIndex = 0;
        }

        protected void OnEnd()
        {
            _animator.OnAnimationDone(Name);
        }
    }
}