using System;
using System.Collections.Generic;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Scenes;
using Microsoft.Xna.Framework.Graphics;

namespace KillerOfCthulhu.Rendering
{
    public class Animator : Component, Updateable, Animateable
    {
        public Dictionary<string, Animation> Animations;

        private Animation _currentAnimation;
        private Action<string> _animationDone; 

        public Animator()
        {
            Animations = new Dictionary<string, Animation>();
        }

        public void PlayAnimation(string animationName, Action<string> onAnimationDone = null)
        {
            _animationDone = onAnimationDone;
            _currentAnimation = Animations[animationName];
            if (!string.IsNullOrEmpty(_currentAnimation.Path))
            {
                GetComponent<RenderableComponent>().Sprite.Texture =
                    SceneManager.ActiveScene.ContentManager.Load<Texture2D>(_currentAnimation.Path);
            }
        }

        public void CreateAnimation(Animation animation)
        {
            Animations.Add(animation.Name, animation);
        }

        public void StopCurrentAnimation()
        {
            _currentAnimation = null;
        }

        public void Update()
        {
            _currentAnimation?.Update();
        }

        public void OnAnimationDone(string animationName)
        {
            _animationDone?.Invoke(animationName);
        }
    }
}