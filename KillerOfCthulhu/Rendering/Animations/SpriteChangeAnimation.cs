using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace KillerOfCthulhu.Rendering.Animations
{
    public class SpriteChangeAnimation : Animation
    {
        private List<Texture2D> _textures;

        public SpriteChangeAnimation(Animator animator, string name, int fps, List<Texture2D> textures) : base(animator, name, fps)
        {
            _textures = textures;
        }

        public override void Update()
        {
            _renderableComponent.Sprite.Texture = _textures[_currentIndex];
        }
    }
}