using System.Collections.Generic;
using KillerOfCthulhu.Rendering.Static;
using Microsoft.Xna.Framework;

namespace KillerOfCthulhu.Rendering
{
    public class SpriteSheetData
    {
        public int YFrame;
        public int XFrame;
        public int Frames;
        public Vector2 Offset;

        public SpriteSheetData(int yFrame, int xFrame, int frames, Vector2? offset = null)
        {
            YFrame = yFrame;
            XFrame = xFrame;
            Frames = frames;
            Offset = offset.GetValueOrDefault();
        }

        public Rectangle[] GetRectangles(int width, int height)
        {
            Rectangle[] rectangles = new Rectangle[Frames];
            for (int i = 0; i < Frames; i++)
            {
                rectangles[i] = new Rectangle((i + XFrame) * width, YFrame, width, height);
            }
            return rectangles;
        }
    }

    public class SpriteSheetAnimation : Animation
    {
        private Vector2 _offset;
        private List<Rectangle> _rectangles;

        public SpriteSheetAnimation(Animator animator, string name, int fps, int width, int height, string path = "", params SpriteSheetData[] spriteSheetDatas) : base(animator, name, fps, path)
        {
            _rectangles = new List<Rectangle>();
            foreach (var spriteSheetData in spriteSheetDatas)
            {
                _rectangles.AddRange(spriteSheetData.GetRectangles(width, height));
            }

            _maxAnimationCount = _rectangles.Count;
        }

        public override void Update()
        {
            base.Update();
            Sprite sprite = _renderableComponent.Sprite;
            sprite.TextureSize = _rectangles[_currentIndex].Size;
            sprite.TexturePoint = _rectangles[_currentIndex].Location;
        }
    }
}