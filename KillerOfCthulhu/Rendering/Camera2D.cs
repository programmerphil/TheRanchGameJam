﻿using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.GameObjectSystem.GameObjects;
using KillerOfCthulhu.Global;
using KillerOfCthulhu.Movement;
using KillerOfCthulhu.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KillerOfCthulhu.Rendering
{
    public class Camera2D : Component
    {
        public float Zoom;
        public Vector2 Origin;
        public Rectangle Bounds => new Rectangle(Position.ToPoint(), Screen.CurrentScreenResolution);

        public Vector2 Position
        {
            get { return Transform.Position; }
            set { Transform.Position = value; }
        }
        public float Rotation => Transform.Rotation;

        private readonly Viewport _viewport;

        public Camera2D(Viewport viewport)
        {
            _viewport = viewport;
            Zoom = 1;
            Origin = new Vector2(_viewport.Width / 2f, _viewport.Height / 2f);
        }

        public Matrix GetViewMatrix()
        {
            return
                Matrix.CreateTranslation(new Vector3(-Position, 0.0f)) *
                Matrix.CreateTranslation(new Vector3(-Origin, 0.0f)) *
                Matrix.CreateRotationZ(Rotation) *
                Matrix.CreateScale(Zoom, Zoom, 1) *
                Matrix.CreateTranslation(new Vector3(Origin, 0.0f));
        }
        
        /// <summary>
        /// Transforms viewport position into world position.
        /// Viewport position is relative to the camera.
        /// Top left is (0, 0)
        /// Bottom right is (1, 0)
        /// Will be clamped between (0, 0) and (1, 1)
        /// </summary>
        /// <param name="viewportPosition">Viewport position to transform into world position</param>
        /// <returns>The transformed world position.</returns>
        public Vector2 ToWorldPosition(Vector2 viewportPosition)
        {
            viewportPosition = MathUtil.Clamp01(viewportPosition);
            return Position + (viewportPosition * Screen.CurrentScreenResolution.ToVector2());
        }

        public Vector2 ToViewportPosition(Vector2 worldPosition)
        {
            return (worldPosition -  Position) / Screen.CurrentScreenResolution.ToVector2();
        }

        public static Camera2D CreateCamera2DWithGameObject(Vector2 position)
        {
            GameObject gameObject = Pool.CreateGameObject(new ObjectCreationProperties(position));
            Camera2D camera2D = new Camera2D(Screen.ViewPort);
            gameObject.AddComponent(camera2D);
            return camera2D;
        }
    }
}