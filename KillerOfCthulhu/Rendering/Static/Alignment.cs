﻿using System;

namespace KillerOfCthulhu.Rendering.Static
{
    [Flags]
    public enum Alignment
    {
        Center = 0,
        Left = 1,
        Right = 2,
        Up = 4,
        Down = 8
    }
}