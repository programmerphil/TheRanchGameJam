﻿using KillerOfCthulhu.GameObjectSystem;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KillerOfCthulhu.Rendering.Static
{
    public interface Drawable
    {
        /// <summary>
        /// Default left up corner(Vector2.Zero). Set via normalized origin.
        /// </summary>
        Vector2 Origin { get; }

        /// <summary>
        /// Default white.
        /// </summary>
        Color DrawColor { get; set; }

        /// <summary>
        /// Default none.
        /// </summary>
        SpriteEffects SpriteEffects { get; set; }

        /// <summary>
        /// Default 0.
        /// </summary>
        float LayerDepth { get; set; }
        Transform Transform { get; set; }

        int Width { get; }
        int Height { get; }
    }
}