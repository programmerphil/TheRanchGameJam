using KillerOfCthulhu.Rendering.Static;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KillerOfCthulhu.Rendering
{
    public class Graphics
    {
        private SpriteBatch _spriteBatch;
        private SpriteFont _font;

        public Graphics(SpriteBatch spriteBatch, SpriteFont font)
        {
            _spriteBatch = spriteBatch;
            _font = font;
        }

        public void Begin(Matrix viewMatrix)
        {
            _spriteBatch.Begin(transformMatrix: viewMatrix, sortMode:SpriteSortMode.FrontToBack, blendState:BlendState.AlphaBlend);
        }

        public void Render(Sprite sprite)
        {
            if (sprite.Transform.Rotation != 0)
            {
                Vector2 scale = new Vector2((float)sprite.Size.GetValueOrDefault().X / sprite.TextureWidth, (float)sprite.Size.GetValueOrDefault().Y / sprite.TextureHeight);
                _spriteBatch.Draw(sprite.Texture, sprite.Transform.Position, null, sprite.SourceRectangle,
                    new Vector2((float)sprite.TextureWidth / 2, (float)sprite.TextureHeight / 2), MathHelper.ToRadians(sprite.Transform.Rotation), scale, sprite.DrawColor, sprite.SpriteEffects, sprite.LayerDepth);
            }
            else
            {
                _spriteBatch.Draw(sprite.Texture, null, sprite.DestinationRectangle, sprite.SourceRectangle, sprite.Origin,
                MathHelper.ToRadians(sprite.Transform.Rotation), sprite.Transform.Scale, sprite.DrawColor, sprite.SpriteEffects, sprite.LayerDepth);
            }
        }

        public void Render(Text text)
        {
            if (text.SpriteFont == null)
            {
                text.SpriteFont = _font;
            }
            _spriteBatch.DrawString(text.SpriteFont ?? _font, text.Message, text.Position, text.DrawColor, MathHelper.ToRadians(text.Transform.Rotation), text.Origin,
                text.Transform.Scale, text.SpriteEffects, text.LayerDepth);
        }

        public void End()
        {
            _spriteBatch.End();
        }
    }
}