namespace KillerOfCthulhu.Rendering
{
    public interface Renderable {

        void Render(Graphics graphics);

    }
}