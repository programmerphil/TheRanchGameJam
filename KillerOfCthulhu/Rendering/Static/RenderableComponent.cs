using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Rendering.Static;
using KillerOfCthulhu.Scenes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace KillerOfCthulhu.Rendering
{
    public abstract class RenderableComponent : Component, Renderable, Animateable
    {
        public Sprite Sprite;
        public bool IsVisible => SceneManager.ActiveScene.Camera2D.Bounds.Intersects(Bounds);

        protected SpriteEffects _spriteEffects;
        protected Texture2D _texture;

        public Rectangle Bounds
        {
            get
            {
                return new Rectangle((int)Transform.Position.X, (int)Transform.Position.Y,(int)(Sprite.Width), (int)(Sprite.Height));
            }
        }

        protected RenderableComponent( Texture2D texture)
        {
            _texture = texture;
        }

        protected RenderableComponent(string pathToSprite, ContentManager contentManager) :
            this(contentManager.Load<Texture2D>(pathToSprite))
        {
        }

        public override void Init(GameObject gameObject)
        {
            base.Init(gameObject);
            Sprite = new Sprite(_texture, Transform);
        }


        public virtual void Render(Graphics graphics)
        {
            if (IsVisible)
            {
                graphics.Render(Sprite);
            }
        }

        public virtual void OnAnimationDone(string animationName){}
    }
}