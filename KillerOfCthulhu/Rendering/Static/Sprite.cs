﻿using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KillerOfCthulhu.Rendering.Static
{
    public class Sprite : Drawable
    {
        public Texture2D Texture;
       
        public Point? Size;

        private const float MaxAlpha = 255;

        private float _layerDepth;
        private float _alpha;
        private Color _drawColor;
        private Color _originalColor;

        public Transform Transform { get; set; }
        public Rectangle DestinationRectangle
        {
            get
            {
                Point? point = Transform?.Position.ToPoint();
                return new Rectangle(point.GetValueOrDefault(), new Point(Width, Height));
            }
        }

        public Rectangle SourceRectangle => new Rectangle(TexturePoint, TextureSize);

        /// <summary>
        /// Default left up corner(Vector2.Zero). Set via normalized origin.
        /// </summary>
        public Vector2 Origin { get; set; }

        /// <summary>
        /// Default white.
        /// </summary>
        public Color DrawColor
        {
            get { return _drawColor; }
            set
            {
                _originalColor = value;
                _drawColor = value;
            }
        }

        /// <summary>
        /// Default none.
        /// </summary>
        public SpriteEffects SpriteEffects { get; set; }

        /// <summary>
        /// Alpha of the sprite. 0 = invisible. 255 = Max alpha.
        /// Is clamped between 0 and 255.
        /// </summary>
        public float Alpha
        {
            get { return (DrawColor.A); }
            set
            {
                _alpha = MathHelper.Clamp(value, 0, MaxAlpha);
                _drawColor = new Color(_originalColor, MaxAlpha) * (_alpha  / MaxAlpha);
            }
        }

        /// <summary>
        /// Default 0.
        /// </summary>
        public float LayerDepth
        {
            get { return _layerDepth; }
            set { _layerDepth = MathUtil.Clamp01(value); }
        }

        public int Width
        {
            get
            {
                var width  =  Size?.X ?? Texture?.Width * Transform?.Scale.X;
                return (int)width.GetValueOrDefault();
            }
        }

        public int Height
        {
            get
            {
                var height = Size?.Y ?? Texture?.Height * Transform?.Scale.Y;
                return (int) height.GetValueOrDefault();
            }
        }

        public Point TextureSize { get; set; }

        public int TextureWidth
        {
            get { return TextureSize.X; }
            set { TextureSize = new Point(value, TextureSize.Y); }
        }

        public int TextureHeight
        {
            get { return TextureSize.Y; }
            set { TextureSize = new Point(TextureSize.X, value); }
        }

        public Point TexturePoint;

        public Vector2 Center => DestinationRectangle.Center.ToVector2();

        /// <summary>
        /// Default left up corner(Vector2.Zero).
        /// Takes texture size in account. So (0.5, 0.5) origin = center of texture.
        /// Is clamped betweeen (0.0) and (1.1)
        /// </summary>
        public Vector2 NormalizedOrigin
        {
            set
            {
                Vector2 normalizedOrigin = new Vector2(MathUtil.Clamp01(value.X), MathUtil.Clamp01(value.Y));
                Origin = new Vector2(normalizedOrigin.X * Width, normalizedOrigin.Y * Height);
            }
        }

        public Sprite(Texture2D texture2D, Transform transform, Vector2? normalizedOrigin = null, Color? drawColor = null,
            SpriteEffects spriteEffects = SpriteEffects.None, float layerDepth = 0, Point? size = null,
            Point? textureSize = null, Point? texturePoint = null)
        {
            Texture = texture2D;
            Transform = transform;
            Size = size;
            NormalizedOrigin = normalizedOrigin.GetValueOrDefault();
            DrawColor = drawColor.GetValueOrDefault(Color.White);
            SpriteEffects = spriteEffects;
            LayerDepth = layerDepth;
            TextureSize = textureSize.GetValueOrDefault(Texture.Bounds.Size);
            TexturePoint = texturePoint.GetValueOrDefault();
            Alpha = MaxAlpha;
        }
    }
}