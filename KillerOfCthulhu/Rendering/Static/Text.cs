﻿using System;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KillerOfCthulhu.Rendering.Static
{
    public class Text : Drawable
    {
        private Alignment _alignment;
        private SpriteFont _spriteFont;
        private float _layerDepth;
        private Sprite _sprite;
        private int _alpha;
        private const int MaxAlpha = 255;

        public string Message;
        public float ExtraLayerDepth = 0.1f;

        public Sprite Sprite
        {
            get { return _sprite; }
            set
            {
                _sprite = value;
                LayerDepth = (Sprite?.LayerDepth + ExtraLayerDepth).GetValueOrDefault(LayerDepth);
            }
        }

        /// <summary>
        /// Alpha of the text. 0 = invisible. 255 = Max alpha.
        /// Is clamped between 0 and 255.
        /// </summary>
        public int Alpha
        {
            get { return (int)(DrawColor.A); }
            set
            {
                _alpha = (int)MathHelper.Clamp(value, 0, MaxAlpha);
                DrawColor *= (float)((float)_alpha / MaxAlpha);
            }
        }

        public Color DrawColor { get; set; }
        public SpriteEffects SpriteEffects { get; set; }

        /// <summary>
        /// Is clamped between 0 and 1.
        /// </summary>
        public float LayerDepth
        {
            get { return _layerDepth; }
            set
            {
                _layerDepth = MathUtil.Clamp01(value);
            }
        }

        public Transform Transform { get; set; }
        public Point TextSize => (_spriteFont?.MeasureString(Message)).GetValueOrDefault().ToPoint();
        public int Width => (int)TextSize.Y;
        public int Height => (int)TextSize.X;

        public SpriteFont SpriteFont
        {
            get { return _spriteFont; }
            set
            {
                _spriteFont = value;
            }
        }

        public Vector2 Origin => CalculateOrigin(_alignment, TextSize.ToVector2(), (Sprite?.DestinationRectangle).GetValueOrDefault());
        public Vector2 Position => (Sprite?.Center).GetValueOrDefault(Transform.Position);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">The message to write</param>
        /// <param name="transform">The transform to take position, scale and rotation from.</param>
        /// <param name="spriteFont">The spritefont, null for default font.</param>
        /// <param name="alignment">How you want the text to align, acoording to an sprite. Irelevant if sending no sprite with.</param>
        /// <param name="drawColor">Color to draw in</param>
        /// <param name="spriteEffects"></param>
        /// <param name="layerDepth">The depth to draw in. 1 = max, 0 = min</param>
        /// <param name="sprite">The sprite to use for positioning.</param>
        public Text(string message, Transform transform, SpriteFont spriteFont = null, Alignment alignment = Alignment.Center, Color? drawColor = null,
            SpriteEffects spriteEffects = SpriteEffects.None, float layerDepth = 0, Sprite sprite = null)
        {
            Message = message;
            Transform = transform;
            SpriteFont = spriteFont;
            LayerDepth = layerDepth;
            DrawColor = drawColor.GetValueOrDefault(Color.White);
            SpriteEffects = spriteEffects;
            Sprite = sprite;
            _alignment = alignment;
            Alpha = MaxAlpha;
        }

        private Vector2 CalculateOrigin(Alignment alignment, Vector2 textSize, Rectangle bounds)
        {
            Vector2 origin = textSize / 2;

            switch (alignment)
            {
                case Alignment.Left:
                    origin.X += (float)bounds.Width / 2  -textSize.X / 2;
                    break;
                case Alignment.Right:
                    origin.X -= (float)bounds.Width / 2 - textSize.X / 2;
                    break;
                case Alignment.Up:
                    origin.Y += (float)bounds.Height / 2  - textSize.Y / 2;
                    break;
                case Alignment.Down:
                    origin.Y -= (float)bounds.Height / 2 - textSize.Y / 2;
                    break;
            }

            return origin;
        }
    }
}