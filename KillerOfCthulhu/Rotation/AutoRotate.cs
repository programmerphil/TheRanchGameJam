﻿using KillerOfCthulhu.GameObjectSystem;

namespace KillerOfCthulhu.Rotation
{
    public class AutoRotate : Component, Updateable
    {
        public void Update()
        {
            Transform.Rotation += 1f;
        }
    }
}