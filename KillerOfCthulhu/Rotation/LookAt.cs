﻿using System;
using KillerOfCthulhu.GameObjectSystem;
using Microsoft.Xna.Framework;

namespace KillerOfCthulhu.Rotation
{
    public class LookAt : Component, Updateable
    {
        private Transform _target;
        private Transform _transformToRotate;

        public LookAt(Vector2 target)
        {
            _target = new Transform(new TransformCreationProperties(target));
        }

        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            _transformToRotate = Transform;
        }

        public void SetTarget(Vector2 target, Transform transformToRotate)
        {
            _target = new Transform(new TransformCreationProperties(target));
            _transformToRotate = transformToRotate;
        }

        public void SetTarget(Transform transform, Transform transformToRotate)
        {
            _target = transform;
            _transformToRotate = transformToRotate;
        }

        public void Update()
        {
            if (_target != null && _transformToRotate != null)
            {
                _transformToRotate.Rotation = CalulateAngle(_target);
            }
        }

        private float CalulateAngle(Transform target)
        {
            Vector2 targetCenter = target.Center;
            var vector2 = targetCenter - Transform.Center;
            var vector1 = new Point(0, -1); // 12 o'clock == 0°, assuming that y goes from bottom to top

            return MathHelper.ToDegrees((float)(Math.Atan2(vector2.Y, vector2.X) - Math.Atan2(vector1.Y, vector1.X)));
        }
    }
}