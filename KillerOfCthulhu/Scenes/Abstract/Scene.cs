using System;
using KillerOfCthulhu.Collision;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.GameObjectSystem.ABE;
using KillerOfCthulhu.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace KillerOfCthulhu.Scenes
{
    public abstract class Scene : Sceneable, Updateable, Renderable
    {
        public Camera2D Camera2D;
        public ContentManager ContentManager;
        public SoundManager SoundManager;
        public bool HasLoaded => _gameObjects.Count > 0;

        protected DelayedList<GameObject> _gameObjects;
        protected LoadingType _loadingType;

        protected Scene(ContentManager contentManager)
        {
            ContentManager = contentManager;
            Camera2D = Camera2D.CreateCamera2DWithGameObject(new Vector2(0, 0));
            SoundManager = new SoundManager(ContentManager);
            _loadingType = LoadingType.Replace;
            _gameObjects = new DelayedList<GameObject>();
            _gameObjects.ItemRemoved += gameObject =>
            {
                gameObject.OnRemoved();
            };
        }

        protected Scene(Scene previusScene) : this(previusScene.ContentManager)
        {
        }

        /// <summary>
        /// Adds a gameobject to the scene.
        /// </summary>
        /// <param name="gameObject">The gameobject to add</param>
        public void Add(GameObject gameObject)
        {
            _gameObjects.AddItem(gameObject);
        }

        /// <summary>
        /// Removes a gameobject from the scene.
        /// </summary>
        /// <param name="gameObject">The gameobject to remove</param>
        public void Remove(GameObject gameObject)
        {
            _gameObjects.RemoveItem(gameObject);
            Collider collider = gameObject.GetComponent<Collider>();
            if (collider != null)
            {
                CollisionManager.Remove(collider);
            }
        }

        public void Update()
        {
            _gameObjects.UpdateList();
            foreach (var gameObject in _gameObjects)
            {
                gameObject.Update();
            }
        }

        public void Render(Graphics graphics)
        {
            graphics.Begin(Camera2D.GetViewMatrix());
            foreach (var gameObject in _gameObjects)
            {
                gameObject.Render(graphics);
            }
            graphics.End();
        }

        #region Scene loading related methods

        /// <summary>
        /// Resets scene.
        /// If loading type is Replace resets everytime scene is loaded.
        /// If loading type is Keep, only resets, if no gameobjects exists.(See HasLoaded property).
        /// </summary>
        public void ResetScene()
        {
            switch (_loadingType)
            {
                case LoadingType.Replace:
                    Reset();
                    break;
                case LoadingType.Keep:
                    if (!HasLoaded)
                    {
                        Reset();
                    }
                    break;
            }
        }

        /// <summary>
        /// Setups scene.
        /// If loading type is Replace setups everytime scene is loaded.
        /// If loading type is Keep, only setups, if no gameobjects exists.(See HasLoaded property).
        /// </summary>
        public void SetupScene()
        {
            switch (_loadingType)
            {
                case LoadingType.Replace:
                    Setup();
                    break;
                case LoadingType.Keep:
                    if (!HasLoaded)
                    {
                        Setup();
                    }
                    break;
            }
        }

        /// <summary>
        /// Loads content. 
        /// If loading type is Replace reloads everytime scene is loaded.
        /// If loading type is Keep, only reloads, if no gameobjects exists.(See HasLoaded property).
        /// </summary>
        public void LoadContent()
        {
            switch (_loadingType)
            {
                case LoadingType.Replace:
                    Load();
                    break;
                case LoadingType.Keep:
                    if (!HasLoaded)
                    {
                        Load();
                    }
                    break;
            }
        }

        /// <summary>
        /// Unloads all gameobjects of the scene(if loading type is Replace) and then calls UnLoad method.
        /// </summary>
        public virtual void UnLoadContent()
        {
            switch (_loadingType)
            {
                case LoadingType.Replace:
                    _gameObjects.RemoveAllItems();
                    UnLoad();
                    break;
            }
        }

        #endregion

        #region Hooks

        /// <summary>
        /// Method for resetting. Is called just before setup.
        /// Does by default set camera position to zero.
        /// Is only called if loading type is Replace.
        /// </summary>
        protected virtual void Reset()
        {
            Camera2D.Position = Vector2.Zero;
        }

        /// <summary>
        /// Method for unloading extra gameobjects. 
        /// All gameobjects added to the scene has been removed, this method is if anything extra needs to be unloaded.
        /// </summary>
        protected virtual void UnLoad() { }

        #endregion

        #region Abstract methods

        /// <summary>
        /// Is meant to setup all the different gameobjects, that are suppposed to be in the scene.
        /// There should however not be added any gameobjects to the scene yet.
        /// </summary>
        protected abstract void Setup();

        /// <summary>
        /// Here you should add all the gameobjects, that you have gotten in the setup method.
        /// </summary>
        protected abstract void Load();

        #endregion
    }
}
