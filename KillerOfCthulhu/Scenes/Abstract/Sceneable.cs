using KillerOfCthulhu.GameObjectSystem;

namespace KillerOfCthulhu.Scenes
{
    public interface Sceneable
    {
        void Add(GameObject gameObject);
        void Remove(GameObject gameObject);
        void LoadContent();
        void SetupScene();
        void UnLoadContent();
    }
}