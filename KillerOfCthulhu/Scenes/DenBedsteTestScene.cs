﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.GameObjectSystem.GameObjects;
using KillerOfCthulhu.Player;
using KillerOfCthulhu.Components.Rendering;
using KillerOfCthulhu.Rendering.Static;
using KillerOfCthulhu.Global;
using KillerOfCthulhu.Components;

namespace KillerOfCthulhu.Scenes
{
    class DenBedsteTestScene : Scene
    {
        private GameObjectDirector _gameObjectDirector;
        private GameObject _monsterSpawner;
        private GameObject _monsterSpawnerSprite;

        public DenBedsteTestScene(ContentManager contentManager) : base(contentManager)
        {
        }

        public DenBedsteTestScene(Scene previusScene) : base(previusScene)
        {
        }

        protected override void Setup()
        {
            _gameObjectDirector = new GameObjectDirector();


            _monsterSpawner = Pool.CreateGameObject(new ObjectCreationProperties((new Vector2(Screen.Width / 2, Screen.Height / 2))));
            _monsterSpawner.AddComponent(new MonsterSpawner());

            _monsterSpawnerSprite = Pool.CreateGameObject(new ObjectCreationProperties((new Vector2(Screen.Width / 2, Screen.Height / 2))));
            _monsterSpawnerSprite.AddComponent(new StaticSprite(new Sprite(ContentManager.Load<Texture2D>("1"), _monsterSpawner.Transform, normalizedOrigin: new Vector2(0.5f, 0.5f),
            size: new Point(120, 120), layerDepth: 0.1f)));




        }

        protected override void Load()
        {
            Add(_monsterSpawner);
        }
    }
}
