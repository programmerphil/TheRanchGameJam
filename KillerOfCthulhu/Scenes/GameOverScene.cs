﻿using BrandonPotter.XBox;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.GameObjectSystem.GameObjects;
using KillerOfCthulhu.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace KillerOfCthulhu.Scenes
{
    public class CheckForGameOver : Component, Updateable
    {
        public void Update()
        {
            foreach (var controller in XBoxController.GetConnectedControllers())
            {
                if (controller.ButtonAPressed)
                {
                    Restart();
                }
            }
        }

        private void Restart()
        {
            SceneManager.Load("TestScene");
        }
    }

    public class GameOverScene : Scene
    {
        private GameObject _gameOverChedck;

        public GameOverScene(ContentManager contentManager) : base(contentManager)
        {
        }

        public GameOverScene(Scene previusScene) : base(previusScene)
        {
        }

        protected override void Setup()
        {
            SoundManager.StopPlayingSong();
            _gameOverChedck =
                Pool.CreateGameObject(new ObjectCreationProperties(Vector2.Zero)).AddComponent(new CheckForGameOver());
        }

      

        protected override void Load()
        {
            Add(_gameOverChedck);
        }
    }
}