﻿namespace KillerOfCthulhu.Scenes
{
    public enum LoadingType
    {
        /// <summary>
        /// Everytime loading scene, get rid of the old and start from new.
        /// </summary>
        Replace,
        /// <summary>
        /// Every time loading scene, only load new stuff, if scene has not been loaded before.
        /// </summary>
        Keep
    }
}