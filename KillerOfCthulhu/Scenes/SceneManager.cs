using System;
using System.Collections.Generic;
using ABE.Scenes;
using KillerOfCthulhu.Rendering;
using Microsoft.Xna.Framework.Content;
using KillerOfCthulhu.UI.Buttons;

namespace KillerOfCthulhu.Scenes
{
    public static class SceneManager
    {
        public static event Action<Scene> LoadedNewScene;
        public static event Action<Scene> StartedLoadingScene;

        public static Scene ActiveScene => _activeScene;

        private static Scene _activeScene;

        private static Dictionary<string, Scene> _scenes; 

        public static void Init(ContentManager contentManager)
        {
            _scenes = new Dictionary<string, Scene>();

            _scenes.Add("DenBedsteTestScene", new DenBedsteTestScene(contentManager));
            _scenes.Add("GameOver", new GameOverScene(contentManager));
            _scenes.Add("TestScene", new TestScene(contentManager));
        }

        public static void Load(Scene newScene)
        {
            _activeScene?.UnLoadContent();
            StartedLoadingScene?.Invoke(newScene);
            newScene.ResetScene();
            newScene.SetupScene();
            newScene.LoadContent();
            _activeScene = newScene;
            _activeScene.Update();
            LoadedNewScene?.Invoke(newScene);
        }

        public static void Load(string sceneName)
        {
            Load(_scenes[sceneName]);
        }
    }
}