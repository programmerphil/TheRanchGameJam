﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ABE.Movement;
using KillerOfCthulhu;
using KillerOfCthulhu.Alien;
using KillerOfCthulhu.Collision;
using KillerOfCthulhu.Components;
using KillerOfCthulhu.Components.Rendering;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.GameObjectSystem.GameObjects;
using KillerOfCthulhu.Global;
using KillerOfCthulhu.Inventory.Display;
using KillerOfCthulhu.Manager;
using KillerOfCthulhu.Movement;
using KillerOfCthulhu.Rendering;
using KillerOfCthulhu.Rendering.Static;
using KillerOfCthulhu.Rotation;
using KillerOfCthulhu.Scenes;
using KillerOfCthulhu.Tank;
using KillerOfCthulhu.Tank2;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using PlayerIndex = KillerOfCthulhu.Tank.PlayerIndex;

namespace ABE.Scenes
{
    public class TestScene : Scene
    {
        public static ScoreManager Player1Score;
        public static ScoreManager Player2Score;

        private GameObject _rail;
        private List<GameObject> _players;
        private GameObject _background;
        private GameObject _boss;
        private GameObject _alienDamageTriggerObject;
        private GameObject _healthText;
        private GameObject _scorePlayer1Text;
        private GameObject _scorePlayer2Text;
        private GameObject _gameOverObject;
        private GameObject _collisionManagerObject;

        public TestScene(ContentManager contentManager) : base(contentManager)
        {
            _loadingType = LoadingType.Replace;
        }

        public TestScene(Scene previusScene) : base(previusScene)
        {
            _loadingType = LoadingType.Replace;
        }

        protected override void Setup()
        {

            CollisionManager.Reset();
            Player1Score = new ScoreManager();
            Player2Score = new ScoreManager();
            HealthManager.Health = HealthManager.StartHealth;
            Player1Score.Score = 0;
            Player2Score.Score = 0;
            _players = new List<GameObject>();
            Circle circle = new Circle(Screen.ToWorldSize(0.52f, 0.535f).ToVector2(), 205);

            _players.Add(CreatePlayer(0, circle.FindClosestPoint(Vector2.Zero), circle, "Tanks/BlueShootAnimation"));
            _players.Add(CreatePlayer(1, circle.FindClosestPoint(Screen.ToWorldPosition(1, 1)), circle, "Tanks/RedShootAnimation"));

            _rail = Pool.CreateGameObject(new ObjectCreationProperties(Screen.ToWorldPosition(0.54f, 0) - new Vector2(Screen.Height / 2, -Screen.ToWorldPosition(0, 0.04f).Y)));
            _rail.AddComponent(new StaticSprite(ContentManager.Load<Texture2D>("Circle"), new Point(Screen.Height, Screen.Height) - Screen.ToWorldSize(0.08f, 0.08f), 0.1f))
                .AddComponent(new BoxCollider2D(true));

            _background = Pool.CreateGameObject(new ObjectCreationProperties(Vector2.Zero));
             _background.AddComponent(new StaticSprite(new Sprite(ContentManager.Load<Texture2D>("Background"),_background.Transform, size:Screen.ToWorldSize(1, 1))));

            _boss = Pool.CreateGameObject(new ObjectCreationProperties(Screen.ToWorldPosition(0.44f, 0.395f)));
            _boss.AddComponent(new StaticSprite(new Sprite(ContentManager.Load<Texture2D>("Boss"), _boss.Transform, new Vector2(0.5f, 0.5f), size:Screen.ToWorldSize(0.16f, 0.24f),
                layerDepth:  0.5f)))
                .AddComponent(new BossAnimation())
                .AddComponent(new MonsterSpawning(ContentManager))
                .AddComponent(new CircleCollider2D(50, true))
                .AddComponent(new Boss());

            _alienDamageTriggerObject =
                Pool.CreateGameObject(new ObjectCreationProperties(Screen.ToWorldPosition(0.5f, 0.5f)));
            _alienDamageTriggerObject.AddComponent(new CircleCollider2D(100, true))
                .AddComponent(new AlienDamageTrigger());
            _healthText = Pool.CreateGameObject(new ObjectCreationProperties(Screen.ToWorldPosition(0.88f, 0.1f)));
            _healthText.AddComponent(new StaticText(new Text("GG", _healthText.Transform, layerDepth:0.25f)))
                .AddComponent(new DisplayShowableText("Health : ", new HealthManager()));

            _scorePlayer1Text = Pool.CreateGameObject(new ObjectCreationProperties(Screen.ToWorldPosition(0.88f, 0.2f)));
            _scorePlayer1Text.AddComponent(new StaticText(new Text("GG", _scorePlayer1Text.Transform, layerDepth: 0.25f)))
                .AddComponent(new DisplayShowableText("P1 Score : ", Player1Score));

            _scorePlayer2Text = Pool.CreateGameObject(new ObjectCreationProperties(Screen.ToWorldPosition(0.88f, 0.3f)));
            _scorePlayer2Text.AddComponent(new StaticText(new Text("GG", _scorePlayer2Text.Transform, layerDepth: 0.25f)))
                .AddComponent(new DisplayShowableText("P2 Score : ", Player2Score));

            _gameOverObject = Pool.CreateGameObject(new ObjectCreationProperties(Vector2.Zero))
                .AddComponent(new GameOver());

            _collisionManagerObject =
                Pool.CreateGameObject(new ObjectCreationProperties(Vector2.Zero)).AddComponent(new CollisionManager());
        }

        protected override void Load()
        {
            SoundManager.PlaySongWithLoop("Alien Metal GameJam");
            foreach (var player in _players)
            {
                Add(player);
            }
            Add(_rail);
            Add(_background);
            Add(_boss);
            Add(_alienDamageTriggerObject);
            Add(_healthText);
            Add(_scorePlayer1Text);
            Add(_scorePlayer2Text);
            Add(_gameOverObject);
            Add(_collisionManagerObject);
        }

        private GameObject CreatePlayer(int controllerNumber, Vector2 startPosition, Circle circle, string picturePath)
        {
            Animator animator = new Animator();
            
            GameObject player = Pool.CreateGameObject(new ObjectCreationProperties(startPosition, tag:Tag.Player));
            player.AddComponent(new PointMover(null))
                .AddComponent(circle)
                .AddComponent(new CircleMover())
                .AddComponent(new Mover(70))
                .AddComponent(new TestCollisionObject())
                .AddComponent(new BoxCollider2D(true))
                .AddComponent(new XboxMover(controllerNumber))
                .AddComponent(new LookAt(Screen.ToWorldPosition(0.5f, 0.5f)))
                .AddComponent(new Shoot(ShootChecks.XboxShootCheck, ContentManager, Tag.Enemy,(PlayerIndex)controllerNumber))
                .AddComponent(animator)
                .AddComponent(new Tank(controllerNumber))
                .AddComponent(new StaticSprite(new Sprite(ContentManager.Load<Texture2D>(picturePath), player.Transform, new Vector2(0.5f, 0.5f),
                size: Screen.ToWorldSize(0.05f, 0.055f), layerDepth:0.75f)));
            animator.CreateAnimation(new SpriteSheetAnimation(animator, "Default", 0, 218, 168, "", new SpriteSheetData(0, 0, 1)));
            animator.CreateAnimation(new SpriteSheetAnimation(animator, "ShootAnimation", 12, 218, 168,"",
                new SpriteSheetData(0, 0, 4), new SpriteSheetData(0, 0, 3)));
            animator.PlayAnimation("Default");
            return player;
        }

        
      
    }
}