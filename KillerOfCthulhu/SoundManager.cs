﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;

namespace KillerOfCthulhu
{
    public class SoundManager
    {
        public static float Volume = 1;
        public static float Pitch = 0;
        public static float Pan = 0;

        private static ContentManager _contentManager;

        public SoundManager(ContentManager contentManager)
        {
            _contentManager = contentManager;
        }

        public static void PlaySong(string assetName)
        {
            PlaySong(_contentManager.Load<Song>(assetName));
        }

        public static void PlaySong(Song song)
        {
            MediaPlayer.Play(song);
        }

        public static void PlaySoundEffect(string assetName)
        {
            PlaySoundEffect(_contentManager.Load<SoundEffect>(assetName));
        }

        public static void PlaySoundEffect(SoundEffect soundEffect)
        {
            soundEffect.Play(Volume, Pitch, Pan);
        }

        public static void StopPlayingSong()
        {
            MediaPlayer.Stop();
        }

        public static void PlaySongWithLoop(string assetName)
        {
            StopPlayingSong();
            MediaPlayer.Play(_contentManager.Load<Song>(assetName));
            MediaPlayer.IsRepeating = true;
        }
    } 
}
