﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BrandonPotter.XBox;
using KillerOfCthulhu.Collision;
using KillerOfCthulhu.Components.Rendering;
using KillerOfCthulhu.Extensions;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.GameObjectSystem.GameObjects;
using KillerOfCthulhu.Global;
using KillerOfCthulhu.Movement;
using KillerOfCthulhu.Rendering;
using KillerOfCthulhu.Rendering.Static;
using KillerOfCthulhu.Scenes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using PlayerIndex = KillerOfCthulhu.Tank.PlayerIndex;
using Screen = KillerOfCthulhu.Global.Screen;
using Timer = KillerOfCthulhu.Global.Timer;

namespace KillerOfCthulhu.Tank2
{
    public delegate bool Check(object[] objects);

    public static class ShootChecks
    {
        public static Check XboxShootCheck = XboxShootCheckMethod;

        private static bool XboxShootCheckMethod(object[] args)
        {
            XBoxController xBoxController = XboxInput.GetXboxController((int) args[0]);
            if (xBoxController != null)
            {
                return xBoxController.TriggerRightPressed;
            }
            return false;
        }
    }

    public class Shoot : Component, Updateable
    {
        private const float StartAttackSpeed = 1;
        private const float StartShootSpeed = 500;

        public float AttackSpeed = StartAttackSpeed;
        public float ShootSpeed = StartShootSpeed;

        private Check _checkDel;
        private bool _canShoot;
        private ContentManager _contentManager;
        private string _shotTag;
        private Animator _animator;
        private PlayerIndex _playerIndex;
        private Dictionary<PlayerIndex, string> PicturePathSet = new Dictionary<PlayerIndex, string>()
        {
            {PlayerIndex.PlayerOne, "Shots/player 1 shot"  },
             {PlayerIndex.PlayerTwo, "Shots/player 2 shot"  }
        };

        private Tank.Tank _tank;

        public Shoot(Check checkDel, ContentManager contentManager, string shotTag, PlayerIndex playerIndex)
        {
            _playerIndex = playerIndex;
            _contentManager = contentManager;
            _checkDel = checkDel;
            _shotTag = shotTag;
            _canShoot = true;
        }

        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            _animator = GetComponent<Animator>();
            _tank = GetComponent<Tank.Tank>();
        }

        public void Update()
        {;
            if (_checkDel(new object[] {(int)_playerIndex}) && _canShoot)
            {
                _canShoot = false;
                SpawnShot();
                _animator.PlayAnimation("ShootAnimation", animationName => _animator.PlayAnimation("Default"));
                Timer.Start(AttackSpeed, () => _canShoot = true);
            }
        }

        private void SpawnShot()
        {
            SoundManager.PlaySoundEffect("Lazer gun sound");
            float angle = Transform.Rotation - 90;
            Vector2 direction = new Vector2((float)Math.Cos(MathHelper.ToRadians(angle)), (float)Math.Sin(MathHelper.ToRadians(angle)));
            Vector2 leftDirection = new Vector2((float)Math.Cos(MathHelper.ToRadians(angle - 90)), (float)Math.Sin(MathHelper.ToRadians(angle - 90)));
            leftDirection.Normalize();
            Animator animator = new Animator();
            GameObject gameObject = Pool.CreateGameObject(
                new ObjectCreationProperties(Transform.Position - leftDirection , tag:Tag.Shot))
                .AddComponent(new Mover(ShootSpeed))
                .AddComponent(new ShotDamage(_shotTag, _playerIndex))
                .AddComponent(new CircleCollider2D(10, true))
                .AddComponent(new ShotMover(direction))
                .AddComponent(animator);
            gameObject.AddComponent(new StaticSprite(_contentManager.Load<Texture2D>(PicturePathSet[_tank.PlayerIndex]),
                new Point(10, 10), 0.25f));
            animator.CreateAnimation(new SpriteSheetAnimation(animator, "Shot", 12, 64,64, "", new SpriteSheetData(0, 0, 3), new SpriteSheetData(64, 0, 2)));
            animator.PlayAnimation("Shot");

            SceneManager.ActiveScene.Add(gameObject);
        }
    }
}