﻿using System;
using ABE.Scenes;
using KillerOfCthulhu.Collision;
using KillerOfCthulhu.Combat;
using KillerOfCthulhu.Components.Rendering;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.GameObjectSystem.GameObjects;
using KillerOfCthulhu.Manager;
using KillerOfCthulhu.Rendering;
using KillerOfCthulhu.Scenes;
using KillerOfCthulhu.Tank;
using Microsoft.Xna.Framework.Graphics;

namespace KillerOfCthulhu.Tank2
{
    public class ShotDamage : Component, TriggerEnter
    {
        public int Damage;

        private string _targetTag;
        private PlayerIndex _playerIndex;

        public ShotDamage(string targetTag, PlayerIndex playerIndex)
        {
            _targetTag = targetTag;
            _playerIndex = playerIndex;
        }

        public void OnTriggerEnter(Collider otherCollider)
        {
            if (otherCollider.GameObject.Tag == _targetTag)
            {
                switch (_playerIndex)
                {
                    case PlayerIndex.PlayerOne:
                        TestScene.Player1Score.Score += 1;
                        break;
                    case PlayerIndex.PlayerTwo:
                        TestScene.Player2Score.Score += 1;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                SceneManager.ActiveScene.Remove(otherCollider.GameObject);
                SceneManager.ActiveScene.Remove(GameObject);
                SoundManager.PlaySoundEffect("Small Impact Sound");
            }
        }
    }
}