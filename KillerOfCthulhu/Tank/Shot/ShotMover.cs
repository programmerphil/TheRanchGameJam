﻿using System;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Movement;
using KillerOfCthulhu.Rendering;
using KillerOfCthulhu.Scenes;
using Microsoft.Xna.Framework;

namespace KillerOfCthulhu.Tank2
{
    public class ShotMover : Component, Updateable
    {
        private const int ShootLength = 1000;

        private Mover _mover;

        private Vector2 _shootDirection;

        public ShotMover(Vector2 shootDirection)
        {
            _shootDirection = shootDirection;
        }

        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            _mover = GetComponent<Mover>();
            Shoot();
        }

        public void Shoot()
        {
            _mover.MoveTo(new Transform(new TransformCreationProperties(Transform.Position + _shootDirection * ShootLength)));
        }

        public void Update()
        {
            if (!GetComponent<RenderableComponent>().IsVisible)
            {
                SceneManager.ActiveScene.Remove(GameObject);
            }
        }
    }
}