﻿using KillerOfCthulhu.Components.Rendering;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Rendering;
using KillerOfCthulhu.Rendering.Static;
using KillerOfCthulhu.Scenes;
using Microsoft.Xna.Framework;
namespace KillerOfCthulhu.Tank
{
    public enum PlayerIndex
    {
        PlayerOne = 0,
        PlayerTwo = 1
    }

    public class Tank : Component
    {
        public PlayerIndex PlayerIndex;

        public Tank(int controllerNumber)
        {
            PlayerIndex = (PlayerIndex) controllerNumber;
        }
    }
}