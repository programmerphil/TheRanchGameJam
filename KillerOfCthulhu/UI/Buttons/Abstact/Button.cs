﻿using System;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Rendering;
using KillerOfCthulhu.Scenes;
using Microsoft.Xna.Framework.Input;

namespace KillerOfCthulhu.UI.Buttons
{
    public class Button : Component, Updateable
    {
        public event Action MouseDown;
        public event Action MouseUp;

        protected RenderableComponent _renderableComponent;

        private MouseState _mouseState;
        private ButtonTriggerState _buttonTriggerState;
        private ButtonState _lastButtonState;

        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            _renderableComponent = GetComponent<RenderableComponent>();
            SceneManager.StartedLoadingScene += OnStartedLoadingScene;
        }

        private void OnStartedLoadingScene(Scene newScene)
        {
            _lastButtonState = Mouse.GetState().LeftButton;
        }

        public virtual void Update()
        {
            _mouseState = Mouse.GetState();
            CheckCollisionWithMouse();
            CallHooks();
            _lastButtonState = _mouseState.LeftButton;
        }

        private void CheckCollisionWithMouse()
        {
            if (_renderableComponent.Bounds.Contains(SceneManager.ActiveScene.Camera2D.Position.ToPoint() + _mouseState.Position))
            {
                SetTriggerState();
                CallMouseButtonHooks();
            }
            else if (_buttonTriggerState == ButtonTriggerState.Staying || _buttonTriggerState == ButtonTriggerState.Entered)
            {
                _buttonTriggerState = ButtonTriggerState.Exiting;
            }
            else
            {
                _buttonTriggerState = ButtonTriggerState.Nothing;
            }
        }

        private void CallMouseButtonHooks()
        {
            switch (_mouseState.LeftButton)
            {
                case ButtonState.Released:
                    if (_lastButtonState == ButtonState.Pressed)
                    {
                        OnMouseUp();
                        MouseUp?.Invoke();
                    }
                    break;
                case ButtonState.Pressed:
                    if (_lastButtonState == ButtonState.Released)
                    {
                        OnMouseDown();
                        MouseDown?.Invoke();
                    }
                    break;
            }
        }

        private void SetTriggerState()
        {

            if (_buttonTriggerState != ButtonTriggerState.Entered && _buttonTriggerState != ButtonTriggerState.Staying)
            {
                _buttonTriggerState = ButtonTriggerState.Entered;
            }
            else if (_buttonTriggerState == ButtonTriggerState.Entered)
            {
                _buttonTriggerState = ButtonTriggerState.Staying;
            }
        }

        private void CallHooks()
        {
            switch (_buttonTriggerState)
            {
                case ButtonTriggerState.Entered:
                    OnEnter();
                    break;
                case ButtonTriggerState.Staying:
                    OnStay();
                    break;
                case ButtonTriggerState.Exiting:
                    OnExit();
                    break;
            }
        }

        #region Hooks

        protected virtual void OnEnter()
        {
        }

        protected virtual void OnStay()
        {
        }

        protected virtual void OnExit()
        {
        }

        public virtual void OnMouseDown()
        {
        }

        public virtual void OnMouseUp()
        {
        }

        #endregion
    }
}