﻿namespace KillerOfCthulhu.UI.Buttons
{
    public enum ButtonTriggerState
    {
        Entered,
        Staying,
        Exiting,
        Nothing
    }
}