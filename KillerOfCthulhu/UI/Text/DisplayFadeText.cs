﻿using System.Collections.Generic;
using KillerOfCthulhu.Components.Rendering;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Global;
using KillerOfCthulhu.Rendering.Static;
using Microsoft.Xna.Framework;

namespace KillerOfCthulhu.Inventory.Display
{
    public class FadeTextInfo
    {
        private const float TickFadeInterval = 0.05f;
        private float _fadeDuration;
        private StaticText _staticText;

        public FadeTextInfo(StaticText staticText, float fadeDuration)
        {
            _staticText = staticText;
            _fadeDuration = fadeDuration;
        }

        public void Start()
        {
            Timer.Start(_fadeDuration, TickFadeInterval, OnFadeTick);
        }

        private void OnFadeTick(float timeGone)
        {
            _staticText.Text.Alpha = (int)((1 - timeGone / _fadeDuration) * 255);
        }
    }

    public class DisplayFadeText : DisplayTextDecorator
    {
        private float _fadeDuration;

        public DisplayFadeText(DisplayText displayText, float fadeDuration) : base(displayText)
        {
            _fadeDuration = fadeDuration;
        }

        public override List<GameObject> Display(List<string> messages)
        {
            List<GameObject> displayedTextObjects = _displayText.Display(messages);

            foreach (var message in displayedTextObjects)
            {
                FadeText(message);
            }

            return displayedTextObjects;
        }

        public void SetSpawnPosition(Vector2 spawnPostion)
        {
            _displayText.PlacementInformation.StartPosition = spawnPostion;
        }

        public override GameObject Display(string message, int index = 0, Alignment alignment = Alignment.Left)
        {
            return FadeText(_displayText.Display(message, index, alignment));
        }

        public override GameObject Display(Text text, int index = 0)
        {
            return FadeText(_displayText.Display(text, index));
        }

        private GameObject FadeText(GameObject objectWithText)
        {
            StaticText staticText = objectWithText.GetComponent<StaticText>();
            FadeTextInfo fadeTextInfo = new FadeTextInfo(staticText, _fadeDuration);
            fadeTextInfo.Start();
            return objectWithText;
        }
    }
}