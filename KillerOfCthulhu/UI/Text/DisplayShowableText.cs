﻿using KillerOfCthulhu.Components.Rendering;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Rendering;

namespace KillerOfCthulhu.Inventory.Display
{
    public class DisplayShowableText : Component, Updateable
    {
        private StaticText _staticText;
        private TextChangeable _textChangeable;
        private string _beforeText;

        public DisplayShowableText(string beforeText, TextChangeable textChangeable)
        {
            _beforeText = beforeText;
            _textChangeable = textChangeable;
        }

        public override void OnAddedToGameObject()
        {
            base.OnAddedToGameObject();
            _staticText = GetComponent<StaticText>();
        }

        public void Update()
        {
            _staticText.Text.Message = _beforeText + _textChangeable.TextValue;
        }
    }
}