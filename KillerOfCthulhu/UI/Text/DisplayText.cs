﻿using System.Collections.Generic;
using KillerOfCthulhu.Components.Rendering;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.GameObjectSystem.GameObjects;
using KillerOfCthulhu.Rendering.Static;
using KillerOfCthulhu.Scenes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KillerOfCthulhu.Inventory.Display
{
    public class DisplayText : Component, TextDisplayable
    {
        public PlacementInformation PlacementInformation;
        private SpriteFont _spriteFont;

        public DisplayText(PlacementInformation placementInformation, SpriteFont spriteFont = null)
        {
            PlacementInformation = placementInformation;
            _spriteFont = spriteFont;
        }

        public List<GameObject> Display(List<string> messages)
        {
            List<GameObject> displayedObjects = new List<GameObject>();

            for (int i = 0; i < messages.Count; i++)
            {
                GameObject textObject = Display(messages[i], i);
                displayedObjects.Add(textObject);
            }

            return displayedObjects;
        }

        public GameObject Display(string message, int index = 0, Alignment alignment = Alignment.Left)
        {
            return Display(new Text(message, null, alignment: alignment));
        }

        public GameObject Display(Text text, int index = 0)
        {
            GameObject textObject = CreateTextObject(index);
            text.Transform = textObject.Transform;
            text.SpriteFont = text.SpriteFont ??  _spriteFont;
            textObject.AddComponent(new StaticText(text));
            return textObject;
        }

        private GameObject CreateTextObject(int index = 0)
        {
            Vector2 spawnPosition = PlacementInformation.GetPositionAt(index);
            return Pool.CreateGameObject(new ObjectCreationProperties(spawnPosition));
        }
    }
}