﻿using System.Collections.Generic;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Rendering.Static;

namespace KillerOfCthulhu.Inventory.Display
{
    public abstract class DisplayTextDecorator :  Component, TextDisplayable
    {
        protected DisplayText _displayText;

        protected DisplayTextDecorator(DisplayText displayText)
        {
            _displayText = displayText;
        }

        public abstract List<GameObject> Display(List<string> messages);
        public abstract GameObject Display(string message, int index = 0, Alignment alignment = Alignment.Left);
        public abstract GameObject Display(Text text, int index = 0);
    }
}