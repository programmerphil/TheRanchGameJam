﻿using Microsoft.Xna.Framework;

namespace KillerOfCthulhu.Inventory.Display
{
    public class PlacementInformation
    {
        public Vector2 SpaceBetweenEachText;
        public Vector2 StartPosition;

        public PlacementInformation(Vector2 spaceBetweenEachText, Vector2 startPosition)
        {
            SpaceBetweenEachText = spaceBetweenEachText;
            StartPosition = startPosition;
        }

        public Vector2 GetPositionAt(int index)
        {
            return StartPosition + SpaceBetweenEachText * index;
        }
    }
}