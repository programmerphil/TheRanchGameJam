using System.Collections.Generic;
using KillerOfCthulhu.GameObjectSystem;
using KillerOfCthulhu.Rendering.Static;

namespace KillerOfCthulhu.Inventory.Display
{
    public interface TextDisplayable
    {
        List<GameObject> Display(List<string> messages);
        GameObject Display(string message, int index = 0, Alignment alignment = Alignment.Left);
        GameObject Display(Text text, int index = 0);
    }
}