﻿using System;
using Microsoft.Xna.Framework;

namespace KillerOfCthulhu.Util
{
    public static class MathUtil
    {
        private static Random _random;

        static MathUtil()
        {
            _random = new Random();
        }

        public static float Clamp01(float value)
        {
            return MathHelper.Clamp(value, 0, 1);
        }

        public static Vector2 Clamp01(Vector2 value)
        {
            return new Vector2(Clamp01(value.X), Clamp01(value.Y));
        }

        public static bool IsRandom(int procent)
        {
            return _random.Next(100) <= procent;
        }

        public static int RandomBetween(int min, int max)
        {
            return _random.Next(min, max);
        }
    }
}